
function breackpoint(){
    return $(window).width() < 920 ? 'mobile' : 'desktop';
}

function column_hide_autodetect(){
    // check if a column is empty, and hide it if it's the case
    let $columns = $(".column");
    $columns.each(function(){
        $(this).show();
        if($(this).children(':visible').length == 0){
            $(this).hide();
        }
    });
}

function update_url_params(){
    let params_array = []
    $(".nav-checkbox").each(function () {
        let section_name = this.id.split("__")[0];
        let is_on = $(this).is(':checked');
        if(is_on){
            params_array.push([section_name, "on"]);
        }
    });
    let new_url = "?"+params_array.map(x => x.join("=")).join("&");
    window.history.pushState("", "", new_url);
}

function section_toggle(checkbox){

    // hide or show the associated section
    let val = $(checkbox).attr("value");

    if($(checkbox).is(':checked')){
        $("details#" + val).attr("open", "");
    } else{
        $("details#" + val).removeAttr("open");
    }

    // update the url accordingly
    update_url_params();

    // correct column hide/show
    column_hide_autodetect();
}

// jquery plugin for shuffling the line-up
(function($){
    $.fn.shuffle = function() {
        var allElems = this.get(),
            getRandom = function(max) {
                return Math.floor(Math.random() * max);
            },
            shuffled = $.map(allElems, function(){
                var random = getRandom(allElems.length),
                    randEl = $(allElems[random]).clone(true)[0];
                allElems.splice(random, 1);
                return randEl;
           });
        this.each(function(i){
            $(this).replaceWith($(shuffled[i]));
        });
        return $(shuffled);
    };
})(jQuery);


$(document).ready(function(){

    let state = breackpoint();

    if (state == 'mobile'){
        $(".column").show();
    }

    // --- activating sections from url parameters
    // example: 
    // index.html/?about="on"&shape="on"
    let params = new URLSearchParams(location.search);

    if (Array.from(params).length != 0){
        $(".nav-checkbox").each(function () {

            let section_name = this.id.split("__")[0];
            let is_on = params.get(section_name);

            let $checkbox = $("#"+section_name+"__checkbox");

            if(is_on){
                $checkbox.prop("checked", true);
                section_toggle($checkbox);
            } else{
                $checkbox.prop("checked", false);
                section_toggle($checkbox);
            }
        });
    }


    // --- line-up shuffling
    $('#line-up__section ul li').shuffle();


    // --- checkbox activation
    let $checkboxes = $('.nav-checkbox');
    $checkboxes.click(function(){
        section_toggle(this);
    });


    // --- on resize, if we go through the breackpoint
    $( window ).resize(function() {

        let old_state = state;
        state = breackpoint();

        // if change from 'mobile' --> 'desktop'
        if (state != old_state && state == 'desktop'){

            // adjust the checkboxes
            $("details.modular-section").each(function(){
                let section_name = $(this).attr("id").split("__")[0];
                let $checkbox = $("#"+section_name+"__checkbox");
                if ($(this).attr("open")){
                    // console.log("check the checkbox for " + section_name);
                    $checkbox.prop("checked", true);
                    section_toggle($checkbox);
                } else{
                    // console.log("uncheck the checkbox for " + section_name);
                    $checkbox.prop("checked", false);
                    section_toggle($checkbox);
                }
            });

            // adjust the column
            column_hide_autodetect();
        }

        // if change from 'desktop' --> 'mobile'
        if (state != old_state && state == 'mobile'){
            $(".column").show();
        }
        
    });

    
    setTimeout(function(){
        $('body').removeClass("preload");
    },1000);

});