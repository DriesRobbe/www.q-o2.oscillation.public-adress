---
title: Agenda
slug: agenda
lang: nl

days:
   
- '26.04':
   day_name: 'Dinsdag'
   events:
   
   - 'Margherita Brillada - Radiophonix':
      streamed: false
      type: 'workshop'
      time: '14:00 - 17:00'
      location: 'Q-O2'
      practical: |
         Geen voorkennis nodig, voertaal Engels.

         Capaciteit: 15 

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/450744690048)
         
         Benodigdheden: koptelefoon, computer, (audio-interface optioneel) 
        
         Meer info: [margherita@q-o2.be](mailto:margherita@q-o2.be)

         Mail naar [info@q-o2.be](mailto:info@q-o2.be) voor groepstarief

      description: |
         Tijdens deze workshop leren de deelnemers de basisprincipes van radiostreamingsoftware en broadcasttechnologie. We verkennen radiokunst via een geschiedkundig overzicht en haar benadering in de huidige tijd, en schenken aandacht aan radio als een pubieke ruimte/plaats voor experimentele geluidskunst. Elke deelnemer brengt geluidsmateriaal (maximum 5 minuten) mee voor een luistersessie en een discussie over de ontwikkeling ervan tot een radiofonisch werk.
      image: './images/photo_2022-04-06_18.54.46.jpeg'
      biography: |
         **Margherita Brillada** is een geluidskunstenaar en elektroakoestische muziekcomponist gevestigd in Den Haag. Door het bewustzijn van het publiek voor radio te vergroten, en door radio op zich te herdenken als een tentoonstellingsruimte voor experimentele geluidskunst, richt haar werk zich op de productie van radiokunstwerken die worden gekenmerkt door stemmen en instrumentale klankrijkheid. Haar praktijk omvat field recordings en soundscapecomposities die door het verkennen van multichannelsystemen meeslepende luisterervaringen creëren, het publiek betrekken en een stem geven aan actuele sociale thema's.
      links: 
      - 'Website artiest': https://margheritabrillada.com/

- '27.04':
   day_name: 'Woensdag'
   events:

   - 'RYBN':
      streamed: false
      type: 'open lab'
      time: '10:00 - 18:00 / op afspraak'
      location: 'Q-O2'
      description: |
         In dit tweedaags open lab plot, troubleshoot en bespreekt RYBN de wandelingen van de komende dagen met lokale gasten. Bezoek is welkom op afspraak (door te mailen naar[info@rybn.org](mailto:info@rybn.org)).
         
         De **Offshore Tour Operator** is een psycho-geografisch GPS prototype dat je door de 800.000 adressen van de ICIJ's Offshore Leaks database leidt. De wandelingen laten deelnemers zoeken naar de fysieke sporen van offshorebankieren binnen de architectuur van verschillende wijken van de stad Brussel. De wandelingen worden zo een echte jacht op lege vennootschappen, trustkantoren, domiciliëringsagentschappen en schaduwfinancieringskantoren en -agenten. Op het einde van elke wandeling biedt een collectieve discussie de deelnemers een platform om hun ervaringen en documenten te delen, om zo collectief een up-to-date beeld van de financiën te vormen die het begrip “offshore” uitdagen.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is een extradisciplinair kunstenaarscollectief, opgericht in 1999 en gevestigd in Parijs.
      links: 
      - 'Website collectief': http://rybn.org/thegreatoffshore/
     
   - 'Lia Mazzari - Whip Cracking':
      streamed: false
      type: 'workshop'
      time: '14:00 - 17:00'
      location: 'Locatie TBA'
      practical: |
         Geen voorkennis nodig, voertaal Engels.

         Capaciteit: 8

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/548020908067)

         Mail naar [info@q-o2.be](mailto:info@q-o2.be) voor groepstarief
      description: |
         Deze workshop leert deelnemers zwepen te hanteren als een sonic-mapping-instrument om architectuur en plekken om ons heen te activeren. We leren over de geschiedenis van zwepen, de terminologie van hun onderdelen en slagen, wat ons helpt om samen een sonische choreografie te componeren van zweepslagen, in interactie met een verscheidene stedelijke en natuurlijke geluiden, architectuur en materialen, om dit alles uit te voeren op zondag 1 mei op het Oscillation Festival. De wisselwerking en polyritmiek van het zwaaien en slagen van zwepen zal dienen als portaal om te luisteren naar ongewone tijden en ritmes. Als intuïtief fenomeen, catharsisch en vaak overdreven gefetisjiseerd verlengstuk van het lichaam en de luisterend oor, produceert de zweepslag een 'sonic boom'. Mee te nemen:
         
         - Kleding met lange mouwen 
         - Bril of zonnebril
         - Pet of hoed
         - Digitale geluidsrecorders/microfoons mee (worden op verschillende plaatsen gezet om de opnames later te synchroniseren)

      image: './images/UGj7yikg.jpeg'
      image_credits: 'copyright ...'
      biography: |
         **Lia Mazzari** activeert nieuw publiek via ontmoetingen met kunst in onconventionele  ruimtes via performance, installatie en interventies. Ze creëert opgenomen en live-evenemen-ten die manieren bespreken waarop geluid kan worden gebruikt als een multidimensionale kracht van veerkracht en akoestisch gemeengoed. Voor deze benadering van geluidsactivisme maakt ze vaak gebruik van omgevingsopnames, instrumenten, stem en transmissietechnologieën. 
      links: 
      - 'Website artiest': https://liamazzari.com/
   - 'Elena Biserna - Feminist Steps':
      streamed: false
      type: 'workshop'
      time: '19:00 - 22:00'
      location: 'Q-O2'
      practical: |
         Voertaal Engels

         Capaciteit: 10 (vrouwen, gender non-conforme- en queerpersonen)

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/405258756791)

         Mail naar [info@q-o2.be](mailto:info@q-o2.be) voor groepstarief
      description: |
         **Avondworkshop voor vrouwen, gender non-conforme- en queerpersonen.**
         
         Vertrekkend vanuit een aantal teksten en protocollen van Pauline Oliveros, het collectief Blank Noise en mijzelf, wil deze workshop een platform zijn om samen na te denken over gender-(luister)ervaringen in de openbare ruimte, en om een aantal gedragingen af te leren die worden verondersteld gepast, veilig of verwacht te zijn als we wandelen. Een aantal eerste stappen om de asymmetrieën in ruimtelijke machtsverhoudingen in vraag te stellen en om samen werkwijzen van zorg, solidariteit, her-eigening of omverwerping te bedenken die andere ruimtelijke ordeningen en gebruiken zouden kunnen inspireren.
      image: './images/KsH5q4fM.jpeg'
      biography: |
         **Elena Biserna** is een onafhankelijke kunsthistorica en gelegenheidscurator gevestigd in Marseille, Frankrijk. Ze schrijft, spreekt, onderwijst, cureert, faciliteert workshops of collectieve projecten, maakt radio en treedt soms op. Haar interesses liggen binnen luisteren en contextuele, op tijd gebaseerde kunstpraktijken die in verband staan met stedelijke dynamiek, socio-culturele processen,en  de publieke en politieke sfeer. Haar teksten verschenen in verschillende internationale publicaties (Les Presses du Réel, Mimesis, Le Mot et le Reste, Errant Bodies, Amsterdam University Press, Cambridge Scholar, Castelvecchi, Bloomsbury, etc.) en tijdschriften.
      links: 
      - 'Website artiest': http://www.q-o2.be/en/artist/elena-biserna/
- '28.04':
   day_name: 'Donderdag'
   events:

   - 'RYBN':
      streamed: false
      type: 'open lab'
      time: '10:00 - 18:00 / op afspraak'
      location: 'Locatie TBA'
      description: |
         In dit tweedaags open lab plot, troubleshoot en bespreekt RYBN de wandelingen van de komende dagen met lokale gasten. Bezoek is welkom op afspraak (door te mailen naar[info@rybn.org](mailto:info@rybn.org)).
         
         De **Offshore Tour Operator** is een psycho-geografisch GPS prototype dat je door de 800.000 adressen van de ICIJ's Offshore Leaks database leidt. De wandelingen laten deelnemers zoeken naar de fysieke sporen van offshorebankieren binnen de architectuur van verschillende wijken van de stad Brussel. De wandelingen worden zo een echte jacht op lege vennootschappen, trustkantoren, domiciliëringsagentschappen en schaduwfinancieringskantoren en -agenten. Op het einde van elke wandeling biedt een collectieve discussie de deelnemers een platform om hun ervaringen en documenten te delen, om zo collectief een up-to-date beeld van de financiën te vormen die het begrip “offshore” uitdagen.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is een extradisciplinair kunstenaarscollectief, opgericht in 1999 en gevestigd in Parijs.
      links: 
      - 'Website collectief': http://rybn.org/thegreatoffshore/

   - 'Alisa Oleva - A Listening Walkshop':
      streamed: false
      type: 'workshop'
      time: '11:00 - 13:00'
      location: 'Q-O2'
      practical: |
         Voertaal Engels.

         Capaciteit: 12

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/232068394712)

         Mail naar [info@q-o2.be](mailto:info@q-o2.be) voor groepstarief
      description: |
         **Giving your ears to: een luister-walkshop**
         
         In deze workshopn luisteren, wandelen, observeren, opmerken en delen we. Hoe klinkt de stad? Welk geluid zou je kiezen om te volgen? Wat zijn de verste geluiden die je kunt horen? Spreekt de stad je aan? We werken een deel van de tijd met z'n allen in groep, een deel z'n tweeën en een deel alleen. Doorlopend zullen er ook momenten zijn om onze ontdekkingen te bespreken.

         **Zie de 'Specials'-rubriek op voor een interactief project van Alisa Oleva.**
      image: './images/dxshKMCw.jpeg'
      biography: |
         **Alisa Oleva** beschouwt de stad als haar studio en het stadsleven als haar werkmateriaal, waarbij ze vraagstukken behandelt rond stedelijke choreografie en archeologie, sporen en oppervlakken, grenzen en inventarissen, intervallen en stiltes en doorgangen en scheuren. Haar projecten hebben zich ontwikkeld als een reeks interactieve situaties, performances, bewegingspartituren, persoonlijke en intieme ontmoetingen, parkour, walkshops en audiowandelingen.
      links: 
      - 'Website artiest': https://www.olevaalisa.com/
   - 'Lia Mazzari':
      streamed: True
      type: 'interjections'
      time: '19:00 - doorlopend'
      location: 'Decoratelier'
      description: |
         **Intermezzi Sounds: Cracks**

         Als intuïtief fenomeen, catharsisch en vaak overdreven gefetisjiseerd verlengstuk van het lichaam en het luisterend oor, produceert de zweepslag een 'sonic boom'. Lia Mazzari zal voorafgaand aan het festival een workshop organiseren waarin de deelnemers een zweep leren hanteren als 'sonic mapping'-instrument om de architectuur en plekken om ons heen te bespelen. Voor deze performance zullen de deelnemers hun geluidschoregrafie van zweepscheuren uitvoeren.
      image: './images/UGj7yikg.jpeg'
      biography: |
         **Lia Mazzari** activeert nieuw publiek via ontmoetingen met kunst in onconventionele  ruimtes via performance, installatie en interventies. Ze creëert opgenomen en live-evenemen-ten die manieren bespreken waarop geluid kan worden gebruikt als een multidimensionale kracht van veerkracht en akoestisch gemeengoed. Voor deze benadering van geluidsactivisme maakt ze vaak gebruik van omgevingsopnames, instrumenten, stem en transmissietechnologieën. 
      links: 
      - 'Website artiest': https://liamazzari.com/
   - 'Hildegard Westerkamp':
      streamed: True
      type: 'luistersessie'
      time: '19:00'
      location: 'Decoratelier'
      description: |
        **_ The Soundscape Speaks - Soundwalking Revisited (2021)_**      
        
        Onze geluidsomgeving heeft ons veel te vertellen over de aankomende klimaatcrisis. Het geeft letterlijk ‘een stem’ aan gebeurtenissen en activiteiten die ons omringen; wanneer we hier echt naar zouden luisteren, zouden we dan ook misschien de diepte kunnen voelen van de ecologische problemen waarmee de wereld wordt geconfronteerd. In het afgelopen jaar besloot ik de vele geluidsopnamen die ik sinds ongeveer 1977/78 heb gemaakt, opnieuw te bekijken. Zonder dat ik het toen wist, vormde die destijds de basis van mijn opnamepraktijk voor vele jaren daarna. Het idee om mijn stem op te nemen terwijl ik de omgeving opneem, was er al vanaf het begin, en was gebaseerd op mijn wens om mijn aanwezigheid bij het opnemen te tonen aan de radioluisteraar. Met andere woorden, door akoestische omgeving en stem samen te laten ‘spreken’, werd een relatie tussen soundscape en menselijke vocale expressie onthuld.

        **Zie de 'Specials'-rubriek op voor een ander project van Hildegard Westerkamp.**
      image: './images/Westerkamp_Banff2a.jpeg'
      biography: |
         **Hildegard Westerkamp** is een Duits-Canadese componist, pedagoog en radiokunstenaar. Sinds het midden van de jaren zeventig werkt ze rond omgevingsgeluid en akoestische ecologie. Haar composities vestigen de aandacht op het luisteren an sich, op de verborgen, inwendige plaatsen van de omgeving waarin we leven, en op (on)bekende details van de akoestiek van de omgeving. Ze publiceerde verschillende artikels en teksten over luisteren, het geluidslandschap en akoestische ecologie en gaf van soundscape workshops internationaal. Zij is stichtend lid van de WFAE
      links: 
      - 'Website artiest': https://www.hildegardwesterkamp.ca/
   - 'Elena Biserna':
      streamed: true
      type: 'talk'
      time: '19:45'
      location: 'Decoratelier'
      description: |
        **Sound walking as a feminist**


        Deze talk behandelt feministische technieken en benaderingen van soundwalking. Vertrekkend vanuit een herinterpretatie van de literatuur over wandelen en vanuit de veronderstelling dat de sociale organisatie van ruimte en tijd niet neutraal zijn maar hegemonische relaties co-produceren - inclusief de patriarchale - bespreek ik de "situated perspectives" (Donna Haraway) die gebaseerd zijn op gender en seksualiteit. Mijn doel is om een aantal benaderingen te bespreken (en promoten) die andere verhalen voortbrengen over wandelen, soundwalking en de openbare ruimte, die de toewijzing van bepaalde lichamen aan bepaalde ruimten in vraag stellen en zo 'world-making' worden - ze verbeelden andere ruimten, 'spaces where things could unfold otherwise' (Leslie Kern).
      image: './images/img_2758.jpg'
      biography: |
         **Elena Biserna** is een onafhankelijke kunsthistorica en gelegenheidscurator gevestigd in Marseille, Frankrijk. Ze schrijft, spreekt, onderwijst, cureert, faciliteert workshops of collectieve projecten, maakt radio en treedt soms op. Haar interesses liggen binnen luisteren en contextuele, op tijd gebaseerde kunstpraktijken die in verband staan met stedelijke dynamiek, socio-culturele processen,en  de publieke en politieke sfeer. Haar teksten verschenen in verschillende internationale publicaties (Les Presses du Réel, Mimesis, Le Mot et le Reste, Errant Bodies, Amsterdam University Press, Cambridge Scholar, Castelvecchi, Bloomsbury, etc.) en tijdschriften.
      links: 
      - 'Artist website': http://www.q-o2.be/en/artist/elena-biserna/
   - 'Enrico Malatesta & Attila Faravelli':
      streamed: true
      type: 'live'
      time: '20:15'
      location: 'Decoratelier'
      description: |     
        **Caveja** is de titel van een grootschalig, open project waarin de mogelijke rol van antie-ke en landelijke technologie bij de constructie van een hedendaags auditief bewustzijn wordt onderzocht. Het werk maakt gebruik van elementen uit de folklore en de rurale rituelen van de Romagnaregio om verschillende artistieke uitingen te bekomen die verband houden met muziek en performatieve kunsten. Centraal in het onderzoek staan de Cavéja Dagli Anëll, een antiek percussief schudinstrument dat in de Romagna gebruikt werd, uitgerust met gestemde metalen ringen, dat zowel bij landbouwwerkzaamheden als bij bijgelovige verzoeningsrituelen gebruikt werd, en de Brontide, een onduidelijk akoestisch fenomeen, vergelijkbaar met het geluid van een aardverschuiving of een explosie in de lucht (skyquakes in het Engels), dat tegenwoordig niet meer hoorbaar is en waarvan de herinnering verloren is gegaan.
       
        Met dank aan MET–Museo degli Usi e Costumi della Gente di Romagna di Santarcangelo / Musei Comunali Santarcangelo Xing, Pollinaria A.I.R 1
      image: './images/PV4ww6ko.jpeg'
      biography: |
         **Enrico Malatesta** is een Italiaanse percussionist en geluidsonderzoeker. Zijn werk situeert zich binnen geluidsinterventie, performance en experimente-le muziek, en verkent de relaties tussen geluid, ruimte en lichamen, de vitaliteit van materialen en de morfologie van oppervlakken. Hij vestigt hierbij de aandacht op percussieve handelingen en verschillende manieren van luisteren.

         **Attila Faravelli** is een Italiaanse geluidskunstenaar en elektroakoestisch muzikant. Binnen zijn praktijk - die field recording, performances, workshops en design omvat - verkent hij de materiële betrokkenheid met de wereld om ons heen.  Hij maakt deel uit van het geluidsonderzoekscollectief Standards in Milaan en is oprichter en curator van het Aural Tools project, een set van eenvoudige objecten om de materiële en conceptuele processen van de geluidsproductiepraktijk van bepaalde muzikanten te documenteren. 

      links: 
      - 'Website Enrico Malatesta': https://enricomalatesta.com/
      - 'Website Attila Faravelli': https://auraltools.tumblr.com/
      - 'Youtube': https://www.youtube.com/watch?v=wGfKNnnOWvE&t=1125s
   - 'Kate Carr':
      streamed: true
      type: 'live'
      time: '21:30'
      location: 'Decoratelier'
      description: |
        **Kate Carr** maakt een soundscape van objecten gevonden in de publieke ruimte in combinate met field recordings van de straatomgeving.
      image: './images/yKfiCDKc.jpeg'
      image_credits: '© Dmitri Djuric'
      biography: |
         Sinds 2010 onderzoekt **Kate Carr** – als artiest en curator - de raakvlakken tussen geluid, plaats, en emotie. Ze onderzocht kleine vissersdorpen in Noord-IJs-land, verkende ondergelopen oevers van de Seine in een stad met een kerncentrale en nam wilde dieren op in Zuid-Afrika en de moerassen van zuidelijke Mexico. Haar werk focust zich op het uitdru-ken van onze relaties met elkaar en de ruimtes waarin we ons bewegen via geluid. Ze legt daarbij de nadruk op de manier waarop geluid onze ervaringen kleurt en hoe we het gebruiken om elkaar, locaties en gebeurtenissen te verbinden, bezetten, onder te dompelen en te verwijderen.
      links: 
      - 'Website artiest': https://www.gleamingsilverribbon.com/
      
   - 'Lázara Rosell Albear':
      streamed: true
      type: 'live'
      time: '22:15'
      location: 'Decoratelier'
      description: |
        Het project **Unsurrounded** begon als een soloperformance waarin geluid, beweging, visuals en woorden met elkaar interageren. Elke voorstelling put uit nieuwe input en nieuwe interacties die kunnen uitmonden in samenwerkingen met andere performers, muzikanten, kunstenaars en allerlei soorten ruimtes en individuen (dieren inbegrepen).  
      image: './images/0af3tq1k.jpeg'
      biography: |
         **Lázara Rosell Albear** is een Cubaans-Belgische kunstenares met een crossmediale praktijk. In haar geluidsonderzoek verkent ze de onconventionele, polyritmische en resonante kwaliteiten van de drumset, zakprompet en Shō gecombineerd met elektronische klanken en bewerkte field recordings.
      links: 
      - 'Website artiest': https://www.mahaworks.org
      
   - 'Davide Tidoni':
      streamed: false
      type: 'live'
      time: '23:00'
      location: 'Decoratelier'
      description: |
         **When Sound Ends** bestaat uit een reeks acties uitgevoerd met microfoons en/of luidsprekers. Elke actie eindigt met de dood van het geluidsapparaat en de daaruit voortvloeien-de verdwijning van het geluid. Het project verkent verscheidende vormen van lichamelijkheid, in relatie tot geluidsproductie en -beluistering, aanraking, en verlies en handelt als een metafoor voor de menselijke conditie en de vergankelijkheid van het bestaan.   
      image: './images/l-o7bRhY.jpeg'
      biography: |
         **Davide Tidoni** is kunstenaar en onafhankelijk onderzoeker en focust op de fysieke, perceptuele en affectieve dimensie van geluid. Zijn werk stelt vragen over interacties met de akoestische ruimte, intersubjectiviteit, vergankelijkheid en lichamelij-ke kwetsbaarheid. Daarnaast werkt hij rond het gebruik van geluid in de tegencul-tuur en politieke strijd, en heeft een veldonderzoek gepubliceerd over de echogroep Brescia 1911 (The Sound of Normalisation, 2018).
      links:
      - 'Website artiest': http://www.davidetidoni.name/
      - 'Vimeo': https://vimeo.com/583036663

- '29.04':
   day_name: 'Vrijdag'
   events:
      
   - 'Attila Faravelli - Aural Tools':
      streamed: false
      type: 'workshop'
      time: '10:00 - 13:00'
      location: 'Q-O2'
      practical: |
         Geen voorkennis nodig, voertaal Engels.

         Capacity: 10

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/454423197265)

         Mail naar [info@q-o2.be](mailto:info@q-o2.be) voor groepstarief
      description: |
         Deze workshop omvat een presentatie en praktische verkenning van de Aural Tools, een reeks objecten die zijn ontworpen om geluid te genereren en uit te sturen op manieren die traditionele opgenomen media (LP's, CD's, digitaal) niet kunnen. Dit zijn vrij eenvoudige apparaten, gemaakt van hout, steen, karton of zeil, die geluid en ruimte, luisteraar en lichaam met elkaar verbinden. Het zijn geen werkstukken van kunstenaars, maar instrumenten die het werkpro-ces documenteren van de kunstenaars die ze hebben gemaakt, en die voor iedereen beschikbaar worden gesteld. 
      image: './images/GCeR-6fQ.jpeg'
      biography: |
         **Attila Faravelli** is een Italiaanse geluidskunstenaar en elektroakoestisch muzikant. Binnen zijn praktijk - die field recording, performances, workshops en design omvat - verkent hij de materiële betrokkenheid met de wereld om ons heen.  Hij maakt deel uit van het geluidsonderzoekscollectief Standards in Milaan.
      links:
      - 'Artist website': https://auraltools.tumblr.com/
   - 'RYBN':
      streamed: false
      type: 'wandeling'
      time: '14:00 - 18:00'
      location: 'Locatie TBA'
      practical: |
         Capaciteit: 20

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/231336394910)
      description: |
         De **Offshore Tour Operator** is een psycho-geografisch GPS prototype dat je door de 800.000 adressen van de ICIJ's Offshore Leaks database leidt. De wandelingen laten deelnemers zoeken naar de fysieke sporen van offshorebankieren binnen de architectuur van verschillende wijken van de stad Brussel. De wandelingen worden zo een echte jacht op lege vennootschappen, trustkantoren, domiciliëringsagentschappen en schaduwfinancieringskantoren en -agenten. Op het einde van elke wandeling biedt een collectieve discussie de deelnemers een platform om hun ervaringen en documenten te delen, om zo collectief een up-to-date beeld van de financiën te vormen die het begrip “offshore” uitdagen.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is een extradisciplinair kunstenaarscollectief, opgericht in 1999 en gevestigd in Parijs.
      links: 
      - 'Website collectief': http://rybn.org/thegreatoffshore/
  
   - 'Alisa Oleva':
      streamed: false
      type: 'wandeling'
      time: '17:00 - 18:00'
      location: 'Atoma'
      practical: |
         Vertrekpunt: Atoma

         [Gratis, registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/399830470766)
      description: |
         **Listening to where you are not** 
         
         We beginnen samen aan een telefoongesprek. Daarna splitsen we ons af en lopen in een willekeurige richting, luisterend naar de beschrijving van wat iemand anders hoort. Zij zullen ergens in Oekraïne zijn. Ergens waar jij niet bent. Ergens waar het geluidslandschap radicaal is veranderd. Brengt die ervaring die plaats naar ons toe? Of brengt het luisteren naar iemands beschrijving van het geluid van een andere plaats ons naar daar?

         **Zie de 'Specials'-rubriek op voor een interactief project van Alisa Oleva.**
      image: './images/AHbrVLYQ.jpeg'
      biography: |
         **Alisa Oleva** beschouwt de stad als haar studio en het stadsleven als haar werkmateriaal, waarbij ze vraagstukken behandelt rond stedelijke choreografie en archeologie, sporen en oppervlakken, grenzen en inventarissen, intervallen en stiltes en doorgangen en scheuren. Haar projecten hebben zich ontwikkeld als een reeks interactie-ve situaties, performances, bewegingspartituren, persoonlijke en intieme ontmoetingen, parkour, walkshops en audiowandelingen. 
      links:
      - 'Website artiest': https://www.olevaalisa.com/
   - 'Collective Actions Group':
      streamed: false
      type: 'live'
      time: '19:00'
      location: 'Atoma'
      description: |
        Deze in situ actie voor Oscillation Festival Oscillation Festival combineert video- en typografie-documentatie gemaakt in het stationsdepot op de Savelovskayalijn in januari 1990, oorspronkelijk gemaakt voor een niet-gerealiseerde installatie van Andrei Monastyrsky. In Oscillation dient deze video, samen met andere documentatie uit de reeks **Trips Out of Town**, als decor voor een leeschoreografie die in samenwerking met de organisatoren van het evenement wordt gerealiseerd. **Het werk wordt uitgevoerd door Sabine Hänsgen, Elena Biserna, Henry Andersen.**
      image: './images/f7V9iaVo.jpeg'
      biography: |
         **Collective Actions Group**, een spil in de ontwikkeling van de Russische conceptuele- en performancekunst, ontstond in 1976. Het collectief werd opgericht door Andrei Monastyrski, Nikita Alexeev, Georgy Kizevalter en Nikolai Panitkov. Latere leden zijn Elena Elagina, Igor Makarevich, Sergei Romashko en Sabine Hänsgen. Decennialang organiseerde de groep **Trips out of Town**, waarbij een leeg veld op het platteland rond Moskou vaak het toneel werd van minimale acties waaraan een hecht netwerk van vrienden en medewerkers deelnam.
      links: 
      - 'Website collectief': https://conceptualism.letov.ru/KD-ACTIONS.htm
   - 'Bill Dietz ':
      streamed: true
      type: 'lecture performance'
      time: '19:30'
      location: 'Atoma'
      description: |
        Als luisteren een rol zou kunnen spelen in het herdenken van de publieke ruim-te, dan moeten eerst de mediatoren, bertrokkenen en normaliserende mechanismen ervan beter verduidelijkt worden. Om dat te bereiken worden de legale, infrastructurele en conceptuele grenzen van het luisteren - zo-wel die van Brussel als die van de zompige overblijfselen van het 'moderne luisteren' als zodanig - hoorbaar gemaakt.
      image: './images/0mNJj6Fk.jpeg'
      biography: |
         **Bill Dietz** is componist, schrijver, en co-voorzitter van het MFA Music/Sound programma van Bard College. Zijn werk werd reeds getoond op verschillende festivals, musea, flatgebouwen, tijdschriften, en op openbare straten. Recente publiceerde hij Maryanne Amacher: Selected Writings and Interviews (co-redactie door Amy Cimini, 2020) en Universal Receptivity (co-redactie door Kerstin Stakemeier, 2021).
      links: 
      - '#brusselssoundmap': https://www.youtube.com/watch?v=96KzBaBuu7A
      - 'Website artiest': https://www.tutorialdiversions.org/
   
   - 'Ryoko Akama & Anne-F Jacques':
      streamed: true
      type: 'live'
      time: '20:00'
      location: 'Atoma'
      description: |
        Warmer wordende filamenten, materialen die langzaam vervormen, thermostaten als controllers: in deze performance verkennen Akama en Jacques hitte als een actieve kracht die rondom ons aan het werk is. Via geluid, licht en nauwelijks merkbare bewegingen wordt temperatuur een waarneembare aanwezigheid.
      image: './images/BgIEAeDk.jpeg'
      biography: |
         **Ryoko Akama** is een Japans-Koreaanse kunstenares die werkt met installatie, performance en compositie. Ze woont momenteel in Huddersfield, UK. **Anne-F Jacques** is een geluidskunstenaar uit Montreal, Canada, die geïnteresseerd is in versterking, onberekenbare apparaten en triviale objecten. Ryoko en Anne-F werken samen sinds 2016, en delen absurde ideeën, mislukte of gevaarlijke experimenten, en occasionele performances en releases.
      links: 
      - 'Bandcamp': https://noticerecordings.bandcamp.com/album/evaporation
      - 'Vimeo': https://vimeo.com/247511818

   - 'Matana Roberts':
      streamed: true
      type: 'live'
      time: '21:15'
      location: 'Atoma'
      image: './images/cURayy2w.jpeg'
      biography: |
         **Matana Roberts** is een internationaal gerenommeerde componiste, bandleider, saxofoniste, geluidsexperimentalist en mixed-media kunstenaar. Roberts is actief binnen uiteenlopende contexten en media, waaronder improvisatie, dans, poëzie en theater. Ze is mogelijks het best bekend voor haar onvolprezen Coin Coin project, een meerdelig werk van "panoramische geluidsweving": het onthult de mystieke wortels en kanaliseert de geestverruimende tradities van de Amerikaanse creatieve expressie, terwijl het een diepgaan-de en substantiële betrokkenheid behoudt met narrativiteit, geschiedenis, samenleving en politieke expressie binnen haar improvisatorische muzikale structuren. 
      links: 
      - 'Website artiest': https://www.matanaroberts.com/
      - 'Bandcamp': https://matana-roberts.bandcamp.com/album/coin-coin-chapter-four-memphis
   - 'Marta de Pascalis':
      streamed: true
      type: 'live'
      time: '22:00'
      location: 'Atoma'
      description: ''
      image: './images/T4f2UB2A.jpeg'
      image_credits: '© Massimo Pegurri'
      biography: |
         **Marta De Pascalis** is een Italiaanse muzikante en sound designer, gevestigd in Berlijn. Haar solowerk maakt gebruik van analoge synthese en een tape-loopsysteem, dat wordt gebruikt om patronen te creëren die een gevoel van dichte, dynamische en catharsische afstand teweegbrengen. Haar sound raakt een breed scala van elektronische muziekgenres aan, waaronder ambient, Berlin school, psychedelica, en tapemuziek.
      links: 
      - 'Website artiest': http://www.martadepascalis.com/
      - 'Bandcamp': https://maesia.bandcamp.com/album/sonus-ruinae
   - 'Open Mic with Francesca Hawker':
      streamed: true
      type: 'live'
      time: '23:00'
      location: 'Atoma'
      description: |
         **The Open Mic Night** zal vermoedelijk de vorm aannemen van een collectieve wandeling over de Expectation Avenue, geleid van de ene performance naar de andere door de vermoedens van Francesca Hawker, die hopelijk enige voorkennis zal hebben over wat er gaat gebeuren. Vermoedelijk duurt het evenement ongeveer een uur (pauzes niet meegerekend) en zullen 5 performers elk 5 minuten de tijd krijgen om het publiek van een onderbouwd en verrassend aanbod te voorzien.

         **Wie wil deelnemen aan de open mic night**, stuurt een voorstel van 1 alinea, samen met de technische vereisten naar **[margherita@q-o2.be](mailto:margherita@q-o2.be)** voor 22 april. Sets zijn beperkt tot 5 minuten en moeten akoestisch zijn, beperkt blijven tot het gebruik van één microfoon of in staat zijn om 'direct-to-mix-er' geïnstalleerd te worden zonder soundcheck ('plug and play'). De performers krijgen een ticket voor het festival voor vrijdag 29/4 en drankbonnen.

      image: './images/to0hn_oc.jpeg'
      image_credits: '© Nikolaj Jessen'
      biography: |
         **Francesca Hawker** is een artieste uit het Verenigd Koninkrijk die in Brussel woont. Ze treedt voornamelijk op met haar stem, opgenomen audio en rudimentaire props en behaalde een Master's degree aan het Dutch Art Institute, een itinerante, rondreizende acad-mie gevestigd in Nederland. Hawker is momenteel artiest in residentie bij MORPHO in Antwerpen. 
      links: 
      - 'Website artiest': https://francescahawker.net/
  
- '30.04':
   day_name: 'Zaterdag'
   events:
      
   - 'RYBN':
      streamed: false
      type: 'wandeling'
      time: '11:00 - 15:00'
      location: 'TBA'
      practical: |
         Capaciteit: 20

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/736161239426)

         Mail naar [info@q-o2.be](mailto:info@q-o2.be) voor groepstarief
      description: |
         De **Offshore Tour Operator** is een psycho-geografisch GPS prototype dat je door de 800.000 adressen van de ICIJ's Offshore Leaks database leidt. De wandelingen laten deelnemers zoeken naar de fysieke sporen van offshorebankieren binnen de architectuur van verschillende wijken van de stad Brussel. De wandelingen worden zo een echte jacht op lege vennootschappen, trustkantoren, domiciliëringsagentschappen en schaduwfinancieringskantoren en -agenten. Op het einde van elke wandeling biedt een collectieve discussie de deelnemers een platform om hun ervaringen en documenten te delen, om zo collectief een up-to-date beeld van de financiën te vormen die het begrip “offshore” uitdagen.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is een extradisciplinair kunstenaarscollectief, opgericht in 1999 en gevestigd in Parijs.
      links: 
      - 'Website collectief': http://rybn.org/thegreatoffshore/
   - 'David Helbich':
      streamed: false
      type: 'participatieve installatie'
      time: '14:00 - 16:00'
      location: 'Abdij van Vorst'
      practical: |
         Geen registratie vereist
      description: | 
         
         **Figures of Walking Together**
         
          De partituur van krijt op gras in tot drie sporen voor een willekeurig aantal mensen is een sociale choreografie, met figuren en patronen geïnspireerd op concepten van institutioneel en intuïtief georganiseerd wandelen in groepen, zoals in dans of militaire dril. Tenslotte is het het individuele en collectieve gedrag van de zichzelf uitvoerende deelnemers dat licht werpt op empowerment binnen dergelijke gegeven structuren. Krijtpartituur & boekje, versie Brussel 2022.

      image: './images/DavidHelbich.jpg'
      image_credits: '© David Helbich'
      biography: | 
         David Helbich is een geluids-, installatie- en performancekunstenaar, die een uiteenlopend scala aan experimentele en conceptuele werken creëert voor podia, koptelefoons, papier en online media, en in de publieke ruimte. Zijn werk beweegt zich tussen representationele, interactieve en conceptuele benaderingen, vaak gericht op concrete fysieke en sociale ervaringen. Een terugkerende interesse bij Helbich is de interactie met een zelfperformend publiek.
      links: 
      - 'Website artiest': http://davidhelbich.blogspot.com/  
      
   - 'Alisa Oleva':
      streamed: false
      type: 'wandeling'
      time: '15:00 - 16:00'
      location: 'Zonneklopper'
      practical: |
         Vertrekpunt: Zonneklopper

         [Gratis, registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/971907879517)
      description: |
         **walk - stop - listen - walk again**

         Een collectieve wandeling volgens een gedeelde partituur. Een uitnodiging om op elkaar af te stemmen, te delen en aandachtig te luisteren. We bewegen als een groep en wanneer één van ons iets hoort waar die voor wil stoppen en wat die wil beluisteren, dan doet die persoon dat, als uitnodiging voor de hele groep om te stoppen en samen te luisteren. We blijven daar luisteren tot iemand anders in de groep besluit om verder te gaan lopen. Dit herhalen we. We weten niet waar we zullen eindigen, maar het luisteren zal ons er brengen.

         **Zie de 'Specials'-rubriek op voor een interactief project van Alisa Oleva.**
      image: './images/mkSDHMHk.jpeg'
      biography: |
         **Alisa Oleva** beschouwt de stad als haar studio en het stadsleven als haar werkmateriaal, waarbij ze vraagstukken behandelt rond stedelijke choreografie en archeologie, sporen en oppervlakken, grenzen en inventarissen, intervallen en stiltes en doorgangen en scheuren. Haar projecten hebben zich ontwikkeld als een reeks interactieve situaties, performances, bewegingspartituren, persoonlijke en intieme ontmoetingen, parkour, walkshops en audiowandelingen.
      links: 
      - 'Website artiest': https://www.olevaalisa.com/

   - 'Pak Yan Lau & Amber Meulenijzer':
      streamed: false
      type: 'live'
      time: '16:00'
      location: 'Abdij van Vorst'
      description: |
        **SAAB SCULPTURES** is een serie werken die de iconische Saab 900 in verschillende contexten plaatst en zo de ruimte waarin ze zich bevindt, transformeert. Doorheen verschillende landschappen worden nieuwe geluidswerken gemaakt in/met de ruimte. Voor Oscillation Festival zullen Amber Meulenijzer en Pak Yan Lau samenwerken om een compositie en live performance te maken op maat van dit geluidssysteem.
      image: './images/HRrOLVm0.jpeg'
      biography: |
         **Pak Yan Lau** is een geluidskunstenaar, improvisator, muzikant en componist, die geboren is in België, roots heeft in Hong Kong en zich nu gevestigd heeft in Brussel. In de loop der jaren ontwikkelde ze een rijk, dicht en intrigerend klankuniversum uit prepared piano's, speelgoedklavieren, synths, elektronica en diverse objecten. Ze vermengt diverse elektro-akoestische benaderingen, verkent geluid op een verbluffende manier en versmelt verschillende geluidsbronnen met poëzie, magie en fijngevoeligheid. 

         **Amber Meulenijzer**  heeft een achtergrond in geluid en beeldende kunst. Ze onderzoekt de spanning tussen deze velden, spelend met aan- en afwezigheid en de rol van stilte en de toeschouwer. Waar ontmoeten decor, installatie en soundscape elkaar? Wat heeft het lichaam nodig om te kunnen luisteren? SAAB SCULPTURES is het eerste project dat dit onderzoek naar de publieke ruimte vertaalt.

      links: 
      - 'Website Amber Meulenijzer': https://ambermeulenijzer.tumblr.com/
      - 'Website Pak Yan Lau': https://pakyanlau.com/              
   - 'De zwarte zusters':
      streamed: false
      type: 'live'
      time: '16:45'
      location: 'Abdij van Vorst'
      description: |
         Een improvisatie naar klank, beeld en het narratief van De zwarte zusters als gemeenschap.
      image: './images/XOyZZSVE.jpeg'
      image_credits: '© Cato Van Rijckeghem'
      biography: |
         Rond-om de naam **De zwarte zusters** heeft een groep jonge mensen zich verzameld. Al improviserend verkennen zij tijdens elk optreden de spanning tussen het muzikale en beeldend performatieve waarbij de zoektocht naar collectieve waarde, naar vriendschap en naar vertrouwen centraal staat. Daarbij laten ze zich graag inspireren door alledaagse voorwerpen, de omgeving en het vrij spel. De performance die u te zien zal krijgen toont een improvisatie naar klank, beeld en het narratief van De zwarte zusters als gemeenschap.
      links:
      - 'Artist website': http://zwartezusters.be/
         
   - 'Round Table':
      streamed: true
      type: 'talk'
      time: '18:00 - 19:00'
      location: 'Zonneklopper'
      description: |
         **Moving (in) Sound**

         Dit ronde tafelgesprek is een gelegenheid om samen te debatteren over formats en apparaturen die de traditionele relaties tussen kunstenaar en publiek destabiliseren door middel van beweging, wandelingen, routes en collectieve of private ruimtelijke exploraties. Hoe genereert dit verschillende manieren om het publiek aan te spreken? Hoe herdefinieert dit de manier waarop we de omgeving waarvan we deel uitmaken bewonen? Op welke manieren kan dit andere ruimtelijke gebruiken en representaties openen? Hoe interfereert dit met de (fysieke, culturele, sociale, politieke) productie van publieke ruimte?

         Met Alisa Oleva, Bill Dietz, RYBN, David Helbich, gemodereerd door Elena Biserna.
      image: './images/Sa_kbAuE.jpeg'
   - 'Céline Gillain':
      streamed: true
      type: 'talk'
      time: '20:00'
      location: 'Zonneklopper'
      description: |
         **How listening is conditioned by context**


         Céline zal het hebben over hoe het luisteren naar muziek wordt bepaald door de context, gebaseerd op haareigen ervaring als performer. Optreden op een muziekfestival, in een kunstinstelling, een concertzaal of een club zijn heel verschillende ervaringen, met verschillende voorwaarden, regels en verwachtingen. Ze is geïnteresseerd in het bevragen van de kloof die dit creëert tussen publieken en hoe deze is geworteld in kwesties met betrekking tot klasse, sexe en ras.
      image: './images/CVtiFED8.jpeg'
      image_credits: '© Claudia Höhne'
      biography: |
         **Céline Gillain** is een kunstenares wiens werk de raakvlakken verkent tussen verschillende kunst- en muziekvormen zoals performance, storytelling, experimentele en elektronische muziek, en daarbij de codes en hiërarchieën die hen beheersen deconstrueert. Haar werk legt de steeds groeiende onzekerheid van de kunstenaar bloot en bevraagt de mechanismen waarmee ideeën worden getransformeerd tot koopwaar in de mainstream cultuur.
      links: 
      - 'Website artiest': https://linktr.ee/CelineGillain  
   - 'Peter Kutin with Stan Maris':
      streamed: true
      type: 'live'
      time: '20:30'
      location: 'Zonneklopper'
      description: |
         Geïmproviseerd concert door Peter Kutin met accordeonist Stan Maris. Het geluid van Maris wordt omgezet en beantwoord door Kutins Light-to-Noise Feedback Systems - geluid en licht zullen schaduwen werpen.
      image: './images/Gh7mDYh0.jpeg'
      biography: |
         **Peter Kutin** werkt met geluid over genres, dragers en media heen. Gedreven door een voortdurende nieuwsgierigheid, ondersteund door een samenwerkingsethos, heeft Kutin muziek en geluidsomgevingen geschreven en ontwikkeld voor film, theater, performance, hedendaagse dans en hoorspelen, en regisseerde ook zelf verschillende experimentele kortfilms.
         
         **Stan Maris** is een belgische accordeonist en componist, hoofdzakelijk actief binnen de improvisatie-scene. Hij speelt en componeert voor Kreis, Ocean Eddie, Erem, MOM en solo accorde-on. Als sideman is Stan actief in bands als Suura, Mathieu Robert Group en Giovanni Di Domenico's New Movement Ensemble.
      links: 
      - 'Website Peter Kutin': http://kutin.klingt.org/
      - 'Website Stan Maris': https://www.stanmaris.com/

   - 'Mariam Rezaei':
      streamed: true
      type: 'live'
      time: '21:30'
      location: 'Zonneklopper'
      description: |
         **BOWN** is experimentele turntablism van componiste en DJ Mariam Rezaei. Met twee draaitafels improviseert Mariam met vocale en instrumentale samples binnen een improvisatie die een brug slaat tussen haar Iraanse en Engelse erfgoed. Timbres bewegen zich tussen reductionisme, noise, free jazz en absurdisme met toetsen van opera en hiphop.
      image: './images/ARTKJWWE.jpeg'
      biography: |
         **Mariam Rezaei** is een gelauwerde componiste, turntablist en performer. Ze leidt het experimente-le kunstproject TOPH in Gateshead, waar ze een vaste reeks concerten en festivals organiseert. Haar nieuwste releases zijn 'SKEEN' op Fractal Meat Cuts, 'The End of The World...Finally' met Sam Goff op Cacophonous Revival Recordings, 'il', een samenwerking met Stephen Bishop op TUSK Editions en 'SISTER' met sopraan Alya Al-Sultani, op Takuroku.
      links: 
      - 'Artist website': https://mariam-rezaei.com/
   - 'Thomas Ankersmit':
      streamed: true
      type: 'live'
      time: '22:15'
      location: 'Zonneklopper'
      description: |
        Thom-as Ankersmit gebruikt PA-systemen en zijn Serge Modular analoge synthesizer om echte, fysieke ruimtes en lichamen te activeren met geluid, of juist om imaginaire ruimtes te suggereren, vaak in contrast met elkaar. Geïnspireerd door het werk van o.a. Maryanne Amacher en Dick Raaijmakers, onderzoekt hij het potentieel van resonerende frequenties en akoestische signalen om eenvoudige stereo PA-systemen te 'hacken' voor een meer fysieke, driedimensionale manier van luisteren. Hoewel hij uitsluitend analoog-elektronische geluiden gebruikt, heeft het resultaat toch vaak een organisch, landschapsachtig karakter.
      image: './images/P1VW05GQ.jpeg'
      image_credits: '© Mich Leemans'
      biography: |
         **Thomas Ankersmit** is een in Berlijn en Amsterdam gevestigde muzikant en geluidskunstenaar. Hij speelt op de Serge Modular synthesizer, zowel live als in de studio. Zijn releases kwamen reeds uit op Shelter Press, PAN en Touch, waarop hij ingewikkelde sonische details en rauwe elektronische energie combineert met een zeer fysieke en ruimtelijke ervaring van geluid.
      links: 
      - 'Artist website': https://thomasankersmit.net/
      - 'Soundcloud': https://soundcloud.com/weerzin/perceptual-geography-excerpt-live-at-ctm-festival-2019
   - 'DJ Marcelle':
      streamed: true
      type: 'live'
      time: '23:00'
      location: 'Zonneklopper'
      image: './images/rhkAjYyk.jpeg'
      biography: |
         'Hoe-wel DJ Marcelle al langer muziek verzamelt dan de meesten van ons in leven zijn, blijft ze een Nederlands lieverdje. Ze wordt gekenmerkd door een ondeugende, regelombuigende en bijna ironische benadering van DJ-en, producen en radio hosten, doorspekt met haar kenmerkende humor. Met haar bekende opstelling met drie draaitafels maakt ze composities vanuit liedjes en symfonieën vanuit mixen - ze laat uiteenlopende genres, eigengemaakte vocale fragmenten en verwrongen soundscapes samensmelten tot een gigantische Frankensteinachtige smeltkroes. Haar optredens zijn inventief, euforisch en bovenal krachtig.' —Resident Advisor
      links:
      - 'Artist website': https://filhounico.com/booking/84

- '01.05':
   day_name: 'Zondag'
   events:
      
   - 'RYBN':
      streamed: false
      type: 'wandeling'
      time: '11:00 - 15:00'
      location: 'Locatie TBA'
      practical: |
         Capaciteit: 20

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/644099568474)

         Mail naar [info@q-o2.be](mailto:info@q-o2.be) voor groepstarief
      description: |
         De **Offshore Tour Operator** is een psycho-geografisch GPS prototype dat je door de 800.000 adressen van de ICIJ's Offshore Leaks database leidt. De wandelingen laten deelnemers zoeken naar de fysieke sporen van offshorebankieren binnen de architectuur van verschillende wijken van de stad Brussel. De wandelingen worden zo een echte jacht op lege vennootschappen, trustkantoren, domiciliëringsagentschappen en schaduwfinancieringskantoren en -agenten. Op het einde van elke wandeling biedt een collectieve discussie de deelnemers een platform om hun ervaringen en documenten te delen, om zo collectief een up-to-date beeld van de financiën te vormen die het begrip “offshore” uitdagen.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is een extradisciplinair kunstenaarscollectief, opgericht in 1999 en gevestigd in Parijs.
      links: 
      - 'Website collectief': http://rybn.org/thegreatoffshore/
   - 'Jérôme Giller':
      streamed: false
      type: 'wandeling'
      time: '13:30 - 15:30'
      location: 'Union Tram Stop'
      practical: |
         Capaciteit: 20

         [Registratie vereist](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/352094840913)

         Mail naar [info@q-o2.be](mailto:info@q-o2.be) voor groepstarief
      description: |
        **Forest Village: Archipelago of Habitats**
        
        In deze wandeling stelt Jérôme Giller voor om de onderkant van Vorst te verken-nen, door woonblokken met elkaar te verbinden die ons toelaten om de historische, sociologische en economische evolutie van het industriegebied te begrijpen, verspreid rond de spoorlijn 124 die Brussel met Charleroi verbindt.

        Startpunt van de wandeling: Tramhalte Union (lijn 82 en 97): Avenue Van Volxem 208 / 1190 Vorst

      image: './images/CC7gWQg0.jpeg'
      image_credits: '© Archives Jérôme Giller'
      biography: |
         **Jérôme Giller** lives and works in Brussels where he leads a reflection on urban and peri-ur-ban territorialities using walking as a method and tool of artistic creation. He surveys the territories following lines of geographical, urban, historical, and poetic wanderings. Giller's interventions are furtive and immateri-al. These are moments to live and experience; laboratories that infiltrate reality.
      links: 
      - 'Artist website': http://www.jeromegiller.net/
   - 'David Helbich':
      streamed: false
      type: 'participatieve installatie'
      time: '14:00 - 16:00'
      location: 'Abdij van Vorst'
      practical: |
         Geen registratie vereist
      description: | 
         
         **Figures of Walking Together** - geen registratie vereist
         
          De partituur van krijt op gras in tot drie sporen voor een willekeurig aantal mensen is een sociale choreografie, met figuren en patronen geïnspireerd op concepten van institutioneel en intuïtief georganiseerd wandelen in groepen, zoals in dans of militaire dril. Tenslotte is het het individuele en collectieve gedrag van de zichzelf uitvoerende deelnemers dat licht werpt op empowerment binnen dergelijke gegeven structuren. Krijtpartituur & boekje, versie Brussel 2022.

      image: './images/DavidHelbich.jpg'
      image_credits: '© David Helbich'
      biography: | 
         David Helbich is een geluids-, installatie- en performancekunstenaar, die een uiteenlopend scala aan experimentele en conceptuele werken creëert voor podia, koptelefoons, papier en online media, en in de publieke ruimte. Zijn werk beweegt zich tussen representationele, interactieve en conceptuele benaderingen, vaak gericht op concrete fysieke en sociale ervaringen. Een terugkerende interesse bij Helbich is de interactie met een zelfperformend publiek.
      links: 
      - 'Website artiest': http://davidhelbich.blogspot.com/  
   - 'Lia Mazzari':
      streamed: false
      type: 'live'
      time: '16:00'
      location: 'Abdij van Vorst'
      description: |
         **Whipping Music**

         Als intuïtief fenomeen, catharsisch en vaak overdreven gefetisjiseerd verlengstuk van het lichaam en het luisterend oor, produceert de zweepslag een 'sonic boom'. Lia Mazzari zal voorafgaand aan het festival een workshop organiseren waarin de deelnemers een zweep leren hanteren als 'sonic mapping'-instrument om de architectuur en plekken om ons heen te bespelen. Voor deze performance zullen de deelnemers hun geluidschoregrafie van zweepscheuren uitvoeren.
      image: './images/UGj7yikg.jpeg'
      biography: |
         **Lia Mazzari** activeert nieuw publiek via ontmoetingen met kunst in onconventionele  ruimtes via performance, installatie en interventies. Ze creëert opgenomen en live-evenemen-ten die manieren bespreken waarop geluid kan worden gebruikt als een multidimensionale kracht van veerkracht en akoestisch gemeengoed. Voor deze benadering van geluidsactivisme maakt ze vaak gebruik van omgevingsopnames, instrumenten, stem en transmissietechnologieën. 
      links: 
      - 'Website artiest': https://liamazzari.com/
   - 'BMB con.':
      streamed: false
      type: 'live'
      time: '16:30'
      location: 'Abdij van Vorst'
      description: |
        BMB con. ontwerpt een nieuwe in-situ voorstelling voor het Oscillation Festival.
      image: './images/zB0NNcvc.jpeg'
      biography: |
         **BMB con.** speelt al meer dan 30 jaar met het evenwicht tussen muziek en noise, geluid en beeld, binnen en buiten, digitaal en analoog, performer en publiek. BMB con. werd in 1989 opgericht door Roelf Toxopeus, Justin Bennett en Wikke 't Hooft. Sinds 2006 bestaat het uit een kernduo dat samenwerkt met een wisselende groep van uitgenodigde kunstenaars en performers. Ze verwerken elektronische en akoestische muziek, film, video en fysiek theater in hun performances en installaties.
      links: 
      - 'Website artiest': http://www.bmbcon.org/
   - 'Labelmarkt':
      streamed: false
      type: 'live'
      time: '17:00 - 21:00'
      location: 'Zonneklopper'
      description: |
         Een label- en en boekenmakrt met onze favoriete lokale onafhankelijke labels en de nomadische boekenwinkel Underbelly.
   - 'RYBN':
      streamed: true
      type: 'luistersessie'
      time: '17:30'
      location: 'Zonneklopper'
      description: |

         RYBN brengt een samenvatting van hun ervaringen van de afgelopen week: het realiseren van de Offshore Tour Operator in Brussel.

         De **Offshore Tour Operator** is een psycho-geografisch GPS prototype dat je door de 800.000 adressen van de ICIJ's Offshore Leaks database leidt. De wandelingen laten deelnemers zoeken naar de fysieke sporen van offshorebankieren binnen de architectuur van verschillende wijken van de stad Brussel. De wandelingen worden zo een echte jacht op lege vennootschappen, trustkantoren, domiciliëringsagentschappen en schaduwfinancieringskantoren en -agenten. Op het einde van elke wandeling biedt een collectieve discussie de deelnemers een platform om hun ervaringen en documenten te delen, om zo collectief een up-to-date beeld van de financiën te vormen die het begrip “offshore” uitdagen.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is een extradisciplinair kunstenaarscollectief, opgericht in 1999 en gevestigd in Parijs.
      links: 
      - 'Website collectief': http://rybn.org/thegreatoffshore/
   - 'Leandro Pisano':
      streamed: true
      type: 'talk'
      time: '18:00'
      location: 'Zonneklopper'
      description: |
       **The Manifesto of Rural Futurism** is een uitnodiging om landelijke omgevingen te ervaren als ruimtes waarin we onze benadering van geschiedenis en landschap in vraag kunnen stellen. In deze tekst, in 2019 geschreven door Leandro Pisano en Beatrice Ferrara, worden luisterpraktijken ingezet als manieren om de 'grensgebieden' van de rurale ruimte kritisch te doorkruisen, waarbij hardnekkige noties over 'onontkoombare marginaliteit', 'residualiteit' en 'periferie' worden uitgedaagd.
      image: './images/oh-uMGU0.jpeg'
      biography: |
         **Leandro Pisano** is curator, schrijver en onafhankelijk onderzoeker die geïnteresseerd is in de raakvlakken tussen kunst, geluid en technocultuur. Zijn onderzoek richt zich specifiek op de politieke ecologie van rurale- en randgebieden. Hij is oprichter van het Interferenze/Liminaria festival en was curator van geluidskunsttentoonstellingen in Italië, Chili en Australië.
      links: 
      - 'Youtube': https://www.youtube.com/watch?v=7WqDmUp9SHU&feature=youtu.be&ab_channel=Liminaria
      - 'Artist website': https://www.leandropisano.it/en/
   - 'Fausto Cáceres (Shirley & Spinoza)':
      streamed: true
      type: 'live'
      time: '18:30'
      location: 'Zonneklopper'
      description: |
         **Street Cries & the Wandering Song** is een poëtische collage van de texturen en het leven die het kleine geplaveide kruispunt onder mijn studioraam doorkruisen in Dali, China. De melodieuze kreten van straatventers, afvalwerkers en spontane scènes werden vastgelegd tussen 2006 en 2020, voornamelijk met een stereomicrofoon opgehangen boven de straat of met binaurale microfoons op andere locaties.
      image: './images/ROGC_sandangel.jpg'
      biography: |
         **Fausto Cáceres** is een Amerikaanse geluidskunstenaar en -verzamelaar die onlangs naar Nieuw-Zeeland verhuisde nadat hij vijftien jaar in de periferie van het vasteland van China had gewoond. Hij documenteerde er de traditionele muziek van minderheidsculturen uitgebreid documenteerde, evenals de steeds veranderende soundscape van China. Cáceres is ook de 'Remote Operator' van de langlopende Shirley & Spinoza Radio.
      links: 
      - 'Artist website': https://beacons.ai/shirleyandspinoza      
   - 'Jasmine Guffond':
      streamed: true
      type: 'live'
      time: '19:00'
      location: 'Zonneklopper'
      description: |
         **Listening to Listening to Listening** is een installatieperformance waarbij Jasmine Guffond cut-ups, willekeurige loops en toonhoogtemanipulaties uitvoert met veldopnames die Margherita Brillada in en rond het Zonnekloppergebouw heeft gemaakt. Via transducers die op specifieke punten in Zonnekloppers Salle Mouvement zijn bevestigd, wordt het potentieel van geluid als trillende kracht geactiveerd om zo de materialiteit van de ruimte zelf te laten resoneren. Geluid dat tegelijkertijd uniek en universeel is, beïnvloedt individuele objecten en materie op een unieke manier, terwijl het iedereen tegelijkertijd omringt met verschillende intensiteiten. Wat zou het kunnen betekenen om collectief, doch niet gelijkwaardig te luisteren, gezien het publiek wordt uitgenodigd om niet alleen naar het gebouw te luisteren, maar ook naar Jasmine die naar Margherita luistert. Zou luisteren naar luisteren een techniek kunnen zijn om verschillen te waarderen?
      image: './images/jasmine-guffond.jpg'
      biography: |
         **Jasmine Guffond** is een kunstenaar en componist die werkt op het kruispunt van sociale, politieke en technische infrastructuren. Haar praktijk is gericht op elektronische compositie in muziek- en kunstcontexten en omvat liveperformances, opnames, installaties en op maat gemaakte browser add-on's. Door middel van de sonificatie van data richt ze zich op het potentieel van geluid om hedendaagse politieke vragen aan te pakkenn en engageert ze zich in het luisteren als een gesitueerde kennispraktijk.
      links: 
      - 'Artist website': http://jasmineguffond.com/
         
   - 'Aymeric de Tapol':
      streamed: true
      type: 'live'
      time: '20:00'
      location: 'Zonneklopper'
      description: |
        Aymeric de Tapol schreef (lees dit luidop in je hoofd): 'Deze muziek is gebaseerd op het beluisteren van een cluster signalen die gecomponeerd werd door geschre-ven sequenties voor analoge synthesizer. Voor mij is het de eerste keer dat de muziek het fenomeen an sich lijkt te zijn; ze staat iets meer in het centrum van haar eigen geschiedenis, en kan altijd in vraag gesteld worden. Samengevat: het is de observatie van een herhaling van bewegende klanken: het polyritme.'
      image: './images/BpzCNIdA.jpeg'
      biography: |
         **Aymeric de Tapol** maakt field recoridngs en muziek die als 'experimenteel' bestempeld worden. Hij werkt samen met andere kunstenaars die actief zijn binnen disciplines als cinema, documentaire, onafhankelijke radio en performancekunst. De Tapol is lid van het duo 'Cancellled' met Yann Leguay en van het collectief 'p-node'. Zijn muziek is uitgebracht door labels als Tanuki, Angström, Vlek, Lexi disques, Tanzprocesz en Knotwilg records.
      links: 
      - 'Bandcamp': https://aymeridetapol.bandcamp.com/track/concert-biennale-du-mans-2022
---
