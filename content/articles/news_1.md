---
title: The Oscillation Bulletin Episode 1
date: 04-28
post_img: http://www.oscillation-festival.be/2021/assets/images/_1200x1600_crop_center-center_100_line/IMG_1241.jpg.webp
section: 
---

Dear all,

If you are read­ing this, it means that you are part of the Q‑O2 cir­cle. The next 10 days we will keep you reg­u­lar­ly updat­ed about a very spe­cial event hap­ping right now: Oscillation Tuned Circuits fes­ti­val.

Unlike what we had hoped, this fes­ti­val is a dis­tanced ver­sion. By this writ­ing, we’d like to engage you into an old school expe­ri­ence that recalls the ear­ly fifties, when peo­ple tuned in col­lec­tive­ly to their favorite radio program.

It’s fair­ly sim­ple to engage: on the 28th of April, at 20:00 you head over to oscil​la​tion​-fes​ti​val​.be, sit com­fort­ably in your couch, press the play but­ton and imag­ine there are thou­sands of peo­ple doing the same. You will tune in to a collective/​individual expe­ri­ence of music, talks, per­for­mances that all address one of the pil­lars of music mak­ing: tun­ing. It’s a now or nev­er expe­ri­ence, none of the broad­casts will be avail­able once the fes­ti­val has end­ed. One advice: tune in, or stay detuned!

For now, to start the excite­ment, we have some good­ies for you: there’s some fea­tures by The Wire Magazine on two of the artists per­form­ing at the fes­ti­val: Jonáš Gruska and People Like Us. Yann Leguay is fea­tured on the It’s Psychedelic Baby Magazine, with an indepth inter­view on his recent works. And next Wednesday Lyl radio will host a spe­cial on the fes­ti­val. Upcoming Friday, at 15:30, De Neus van God, at the infa­mous Radio Panik, will host Henry Andersen, who will give a deep insight in the what, who, and why of the festival.

Last, but not least: from upcom­ing Friday, the 23rd until the 28th of April, there’s an exten­sive pro­gram of work­shops — both off- and online — address­ing top­ics like a.o. synth-like vocal explo­rations, live cod­ing exper­tise, col­lec­tive impro­vis­ing and even spring rolls sound med­i­ta­tion. You can find all info at the site; places are lim­it­ed, so be quick to subscribe!

On a prac­ti­cal note: Stellan Veloce, who facil­i­tates a work­shop and who per­forms on the fes­ti­val, is look­ing for ​“spare” har­mon­i­cas, lying some­where in the base­ment or attic, to donate or sell! Please con­tact us!

Kind regards,

The Oscillation Crew
