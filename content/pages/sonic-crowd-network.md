---
title: Public Address
slug: shape
default_state: 'on'
column: 1
order: 1
overflow: none
---

<h1><a href="http://www.oscillation-festival.be/2022/">Oscillation <br/>Public Address</a></h1>

<div class="svg-wrapper">
    <div>
      <svg id="sonic-crowd-network__svg" viewbox="0 0 1000 1000" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg">
          <path d=""/>
      </svg>
      <p id="sonic-crowd-network__infos">
        There are <span id="number-of-user"></span> persons connected
      </p>
    </div>
</div>

<h2 class="festival-date">26.04 → 01.05.2022</h2>