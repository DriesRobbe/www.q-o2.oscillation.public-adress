---
title: Agenda
slug: agenda
default_state: 'on'
column: 2
order: 3
overflow: scroll

days:
   
- '26.04':
   day_name: 'Tuesday'
   events:
   
   - 'Margherita Brillada - Radiophonix':
      streamed: false
      type: 'workshop'
      time: '14:00 - 17:00'
      location: 'Q-O2'
      practical: |
         No prior knowledge necessary, English spoken.

         Capacity: 15 

         [Registration required](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/450744690048)
         
         Required Equipment: headphones, computer, (audio interface optional) 
                  
         More info: [margherita@q-o2.be](mailto:margherita@q-o2.be)

         For a group price, please email [info@q-o2.be](mailto:info@q-o2.be)

      description: |
         During this workshop, we will explore the territory of radio art with an overview of its history and approach in the contemporary era, giving an insight into radio as a public space/place for experimental sound art. Moving into the basics of broadcasting technology, we will take a look at the main radio streaming software. Each participant should bring sound materials (maximum of 5 minutes) for a listening session and an open discussion on developing them for a radiophonic piece. More information on registration.
      image: './images/photo_2022-04-06_18.54.46.jpeg'
      biography: |
         **Margherita Brillada** is a sound artist and electroacoustic music composer based in Den Haag. By increasing audiences' awareness and re-thinking radio as an exhibition space for experimental Sound Art, her work focuses on the production of radio artworks characterized by voices and instrumental sonorities. Her practice involves field recording and soundscape composition by exploring multichannel systems to create immersive listening experiences, engage with audiences, and bring a voice to current social thematics.
      links: 
      - 'Artist website': https://margheritabrillada.com/

- '27.04':
   day_name: 'Wednesday'
   events:

   - 'RYBN':
      streamed: false
      type: 'open lab'
      time: '10:00 - 18:00 / on appointment'
      location: 'Q-O2'
      description: |
         In this two-day open lab, RYBN will plot, trouble-shoot and discuss the walks of the coming days with local invitees. Public visits are welcome with appointment (by e-mailing to [info@rybn.org](mailto:info@rybn.org)).
         
         The **Offshore Tour Operator** is a psycho-geographic GPS prototype that guides you through the 800,000 addresses of the ICIJ's Offshore Leaks database. The walks bring the participants to search for the physical traces of offshore banking within the architecture of various neighborhoods of the city of Brussels. Thus, the walks transform into a real hunt for shell companies, trust firms, domiciliation agencies and shadow finance offices and agents. At the end of each walk, a collective discussion offers a platform to participants to share their experiences and documents, in order to shape collectively an up to date image of finance that challenges the very notion of 'offshore'.

      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is an extradisciplinary artist collective, created in 1999 and based in Paris.
      links: 
      - 'Collective website': http://rybn.org/thegreatoffshore/
     
   - 'Lia Mazzari - Whip Cracking':
      streamed: false
      type: 'workshop'
      time: '14:00 - 17:00'
      location: 'Location TBA'
      practical: |
         No prior knowledge necessary, English spoken.

         Capacity: 8

         [Registration required](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/548020908067)

         For a group price, please email [info@q-o2.be](mailto:info@q-o2.be)
      description: |
         This workshop teaches participants to swing and crack whips as an investigatory sonic mapping device to activate the architectures and sites around us. We will learn about the history of whips, the terminology of their body parts and cracks, which will help us to co-compose a sonic choreography of whip cracks in interaction with a variety of urban and natural sounds, architectures, and materials, with a view to reperforming this score on Sunday May 1st. The embodied interplay and polyrhythmia of the swinging and cracking of whips will be a portal to listen to our uncommon times and rhythms. A visceral phenomenon, cathartic and often overly fetishized extension of the body and listening machine, the whip crack produces a sonic boom. All levels welcome, and be sure to:

         - Wear long sleeved clothing
         - Bring some glasses/goggles/sunglasses
         - Bring a cap/hat/brimmed hat
         - Bring your digital sound recorders/microphones (we will place them in different locations and sync the recordings later)

      image: './images/UGj7yikg.jpeg'
      biography: |
         **Lia Mazzari** engages new audiences through encounters with art in non-conventional spaces via performance, installation and intervention. She creates recorded/live events that discuss ways in which sound can be used as a multidimensional force of resilience and acoustic commoning. This relationship towards a sonic activism often engages environmental recording, instrumentation, voice and transmission technologies.
      links: 
      - 'Artist website': https://liamazzari.com/
   - 'Elena Biserna - Feminist Steps':
      streamed: false
      type: 'workshop'
      time: '19:00 - 22:00'
      location: 'Q-O2'
      practical: |
         English spoken

         Capacity: 10 (women, gender non conforming and queer people)

         [Registration required](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/405258756791)

         For a group price, please email [info@q-o2.be](mailto:info@q-o2.be)
      description: |
         **Night workshop for women, gender non conforming and queer people.**
         
         Starting from some text scores and protocols by Pauline Oliveros, the Blank Noise collective and myself, this workshop aims to be a platform to reflect together on gendered (listening) experiences in public space and to unlearn some of the behaviours that are assumed as appropriate, safe or expected when we walk. Some first steps to question the asymmetries in spatial power relations and to imagine together practices of care, solidarity, re-appropriation or overturning that might feed other spatial configurations and practices.
      image: './images/KsH5q4fM.jpeg'
      biography: |
         **Elena Biserna** is an independent art historian and occasional curator based in Marseille, France. She writes, talks, teaches, curates, facilitates workshops or collective projects, makes radio and sometimes performs. Her interests are focused on listening and on contextual, time-based art practices in relationship with urban dynamics, socio-cultural processes, the public and political sphere. Her writings have appeared in several international publications (Les Presses du Réel, Mimesis, Le Mot et le Reste, Errant Bodies, Amsterdam University Press, Cambridge Scholar, Castelvecchi, Bloomsbury, etc.) and journals.
      links: 
      - 'Artist website': http://www.q-o2.be/en/artist/elena-biserna/
- '28.04':
   day_name: 'Thursday'
   events:

   - 'RYBN':
      streamed: false
      type: 'open lab'
      time: '10:00 - 18:00 / on appointment'
      location: 'Q-O2'
      description: |
         In this two-day open lab, RYBN will plot, trouble-shoot and discuss the walks of the coming days with local invitees. Public visits are welcome with appointment (by e-mailing to [info@rybn.org](mailto:info@rybn.org)).
         
         The **Offshore Tour Operator** is a psycho-geographic GPS prototype that guides you through the 800,000 addresses of the ICIJ's Offshore Leaks database. The walks bring the participants to search for the physical traces of offshore banking within the architecture of various neighborhoods of the city of Brussels. Thus, the walks transform into a real hunt for shell companies, trust firms, domiciliation agencies and shadow finance offices and agents. At the end of each walk, a collective discussion offers a platform to participants to share their experiences and documents, in order to shape collectively an up to date image of finance that challenges the very notion of 'offshore'.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is an extradisciplinary artist collective, created in 1999 and based in Paris.
      links: 
      - 'Collective website': http://rybn.org/thegreatoffshore/

   - 'Alisa Oleva - A Listening Walkshop':
      streamed: false
      type: 'workshop'
      time: '11:00 - 13:00'
      location: 'Q-O2'
      practical: |
         English spoken

         Capacity: 12

         [Registration required](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/232068394712)

         For a group price, please email [info@q-o2.be](mailto:info@q-o2.be)
      description: |
         **Giving your ears to: a listening walkshop**
         
         We will spend time listening, walking, observing, noticing, sharing. How does the city sound? Which sound would you choose to follow? What are the furthest sounds you can hear? Does the city address you? We will work part of the time as a group all together, part of the time in pairs and there will also be some time for you to explore things on your own. Throughout, there will be moments to discuss our discoveries.


         **See the 'Specials' section of the festival website for an interactive project by Alisa Oleva.**
      image: './images/dxshKMCw.jpeg'
      biography: |
         **Alisa Oleva** treats the city as her studio and urban life as material, considering issues of urban choreography and urban archeology, traces and surfaces, borders and inventories, intervals and silences, passages and cracks. Her projects have manifested as a series of interactive situations, performances, movements scores, personal and intimate encounters, parkour, walkshops, and audio walks.
      links: 
      - 'Artist website': https://www.olevaalisa.com/
   - 'Lia Mazzari':
      streamed: true
      type: 'interjections'
      time: '19:00 - throughout'
      location: 'Decoratelier'
      description: |
         **Intermezzi Sounds: Cracks**

         An unconventional device for sound production, turned-into instrument. A visceral phenomenon, cathartic and often overly fetishized extension of the body and listening machine, the whip crack produces a sonic boom. A series of very short interjections throughout the evening in dialogue with the architecture.
      image: './images/UGj7yikg.jpeg'
      biography: |
         **Lia Mazzari** engages new audiences through encounters with art in non-conventional spaces via performance, installation and intervention. She creates recorded/live events that discuss ways in which sound can be used as a multidimensional force of resilience and acoustic commoning. This relationship towards a sonic activism often engages environmental recording, instrumentation, voice and transmission technologies.
      links: 
      - 'Artist website': https://liamazzari.com/

   - 'Hildegard Westerkamp':
      streamed: True
      type: 'listening session'
      time: '19:00'
      location: 'Decoratelier'
      description: |
        **_ The Soundscape Speaks - Soundwalking Revisited (2021)_**      
        
        The sound environment has much to tell us about the coming climate crisis. It simply 'voices' all activities—and if we dare to really listen, we may sense the depth of the environmental trouble the world is facing. During the past year I decided to re-examine the many sound recordings I have made since approximately 1977/78. Without knowing it then, it formed the base of my recording approach for many years to come. The idea of including my voice while recording the environment was there from the start and was based in the desire to acknowledge my recording presence to the radio listener. In other words, in speaking together – acoustic environment and voice – a relationship between soundscape and human vocal expression was revealed.


        **See the 'Specials' section of the festival website for a further work by Hildegard Westerkamp.**
      image: './images/Westerkamp_Banff2a.jpeg'
      biography: |
         **Hildegard Westerkamp** is a German Canadian composer, educator and radio artist whose work since the mid-seventies has centre around environmental sound and acoustic ecology. Her compositions draw attention to the act of listening itself, to the inner, hidden spaces of the environments we inhabit and to details both familiar and foreign in the acoustic environment. She has written numerous articles and texts addressing issues of the soundscape, acoustic ecology and listening, has travelled widely, giving lectures and conducting soundscape workshops internationally. She is a founding member of the WFAE.
      links: 
      - 'Artist website': https://www.hildegardwesterkamp.ca/
   - 'Elena Biserna':
      streamed: true
      type: 'talk'
      time: '19:45'
      location: 'Decoratelier'
      description: |
        **Sound walking as a feminist**

        This talk focuses on feminist practices and approaches in soundwalking. Starting from a re-reading of the literature on walking and from the assumption that the social organization of space and time are not neutral but co-produce hegemonic relationship, including the patriarchal ones, I look at 'situated perspectives' (Donna Haraway) grounded on gender and sexuality. My aim is to put in dialogue (and to promote) an array of approaches that engender other narratives of walking, soundwalking and public space, that challenge the assignation of certain bodies to certain spaces and thus become 'world-making' – they envision other spaces, 'spaces where things could unfold otherwise' (Leslie Kern).
      image: './images/img_2758.jpg'
      biography: |
         **Elena Biserna** is an independent art historian and occasional curator based in Marseille, France. She writes, talks, teaches, curates, facilitates workshops or collective projects, makes radio and sometimes performs. Her interests are focused on listening and on contextual, time-based art practices in relationship with urban dynamics, socio-cultural processes, the public and political sphere. Her writings have appeared in several international publications (Les Presses du Réel, Mimesis, Le Mot et le Reste, Errant Bodies, Amsterdam University Press, Cambridge Scholar, Castelvecchi, Bloomsbury, etc.) and journals.
      links: 
      - 'Artist website': http://www.q-o2.be/en/artist/elena-biserna/
   - 'Enrico Malatesta & Attila Faravelli':
      streamed: true
      type: 'live'
      time: '20:15'
      location: 'Decoratelier'
      description: |     
        **Caveja** is the title of a large, open project investigating the possible role of ancient/rural technology in the construction of a contemporary aural awareness. The work appropriates elements of folklore and rural Romagna rituality in order to produce multiple artistic outputs that deal with music and performative arts. Central to the research is the Cavéja Dagli Anëll, an ancient sound shaking device used in Romagna, equipped with tuned metal rings and used both in agricultural work and in particular superstitious-propitiatory rituals, and the Brontide, an unexplained acoustic phenomenon, similar to the sound of a landslide or an explosion in the sky (in English as skyquakes), nowadays no longer audible, and of which memory has been lost.
       
        Thanks to MET–Museo degli Usi e Costumi della Gente di Romagna di Santarcangelo / Musei Comunali Santarcangelo Xing, Pollinaria A.I.R 1
      image: './images/PV4ww6ko.jpeg'
      biography: |
         **Enrico Malatesta** is an Italian percussionist and sound researcher active in the field of experimental music, sound intervention and performance; his practice explores the relations between sound, space and body, the vitality of materials and the morphology of surfaces, with particular attention to the percussive acts and the modes of listenings.

         **Attila Faravelli** is an Italian sound artist and electro-acoustic musician. Within his practice - which encompasses field recording, performances, workshops and design - he explores the material involvement with the world around us. He is part of the sound research collective Standards in Milan. He is founder and curator for the Aural Tools project, a series of simple objects to document.
      links: 
      - 'Website Enrico Malatesta': https://enricomalatesta.com/
      - 'Website Attila Faravelli': https://auraltools.tumblr.com/
      - 'Youtube': https://www.youtube.com/watch?v=wGfKNnnOWvE&t=1125s
   - 'Kate Carr':
      streamed: true
      type: 'live'
      time: '21:30'
      location: 'Decoratelier'
      description: |
        Kate Carr will focus on building a soundscape from objects found in public space, combined with field recordings of the streetscape.
      image: './images/yKfiCDKc.jpeg'
      image_credits: '© Dmitri Djuric'
      biography: |
         Since 2010, **Kate Carr** has been investigating the intersections between sound, place, and emotionality as both an artist and a curator. During this time she has ventured from tiny fishing villages in northern Iceland, explored the flooded banks of the Seine in a nuclear power plant town, recorded wildlife in South Africa, and in the wetlands of southern Mexico. Her work centres on articulating our relationships with each other and the spaces we move through using sound, with a focus on the ways sound contours our experiences of the world; how we deploy it to connect, occupy, immerse and remove ourselves from locations, events and eachother.
      links: 
      - 'Artist website': https://www.gleamingsilverribbon.com/
      
   - 'Lázara Rosell Albear':
      streamed: true
      type: 'live'
      time: '22:15'
      location: 'Decoratelier'
      description: |
        The project **Unsurrounded**, started as a solo performance project in which sound, movement, visuals and words collide. Each presentation draws on new inputs and new interactions that can extent to collaborations with other performers, musicians, artists and all kind of spaces and individuals (animals included).
      image: './images/0af3tq1k.jpeg'
      biography: |
         **Lázara Rosell Albear** is Cuban-Belgian artist with a crossmedia practice. In her sound research she explores the odd’s, polyrythmical and resonant qualities of the drumset, pocket trumpet and sho combined with electronic sounds and manipulated field recordings.
      links: 
      - 'Artist website': https://www.mahaworks.org
      
   - 'Davide Tidoni':
      streamed: true
      type: 'live'
      time: '23:00'
      location: 'Decoratelier'
      description: |
         **When Sound Ends** consists of a series of actions performed with microphones and/or loudspeakers. Each action ends with the death of the sound device and the consequent disappearance of sound. The project explores themes of corporeality, in relation to sound production and audition, touch, and loss and deals with the presence and absence of sound as a metaphor for the human condition and the transitory nature of existence.
      image: './images/l-o7bRhY.jpeg'
      biography: |
         **Davide Tidoni** is an artist and independent researcher. He is interested in the physical, perceptual and affective dimension of sound. His works address questions about interaction with acoustic space, intersubjectivity, impermanence, and bodily frailty. He is also interested in the use of sound in counter-culture and political struggle and has published a field research on the ultras group Brescia 1911 (The Sound of Normalisation, 2018).
      links:
      - 'Artist website': http://www.davidetidoni.name/
      - 'Vimeo': https://vimeo.com/583036663

- '29.04':
   day_name: 'Friday'
   events:
      
   - 'Attila Faravelli - Aural Tools':
      streamed: false
      type: 'workshop'
      time: '10:00 - 13:00'
      location: 'Q-O2'
      practical: |
         No prior knowledge necessary, English spoken.

         Capacity: 10

         [Tickets](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/454423197265)

         For a group price, please email [info@q-o2.be](mailto:info@q-o2.be)
      description: |
         The workshop offers a presentation and a practical exploration of the Aural Tools, which consist in a series of objects designed to produce and broadcast sound in ways that traditional recorded media (LPs, CDs, digital) cannot. They are rather simple devices, made of wood, stone, cardboard or tarp, which link sound and space, listener and body. They are not artists' pieces, but tools that document the working process of the artists who made them, and are made available for everyone's use. Aural Tools was founded by Attila Faravelli, who is also its curator.
      image: './images/GCeR-6fQ.jpeg'
      biography: |
         **Attila Faravelli** is an Italian sound artist and electro-acoustic musician. Within his practice – which encompasses field recording, performances, workshops and design – he explores the material involvement with the world around us. He is also part of the sound research collective Standards in Milan.
      links: 
      - 'Artist website': https://auraltools.tumblr.com/
   - 'RYBN':
      streamed: false
      type: 'walk'
      time: '14:00 - 18:00'
      location: 'Location TBA'
      practical: |
         Capacity: 20

         [Registration required](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/231336394910)
      description: |
        The **Offshore Tour Operator** is a psycho-geographic GPS prototype that guides you through the 800,000 addresses of the ICIJ's Offshore Leaks database. The walks bring the participants to search for the physical traces of offshore banking within the architecture of various neighborhoods of the city of Brussels. Thus, the walks transform into a real hunt for shell companies, trust firms, domiciliation agencies and shadow finance offices and agents. At the end of each walk, a collective discussion offers a platform to participants to share their experiences and documents, in order to shape collectively an up to date image of finance that challenges the very notion of 'offshore'.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is an extradisciplinary artist collective, created in 1999 and based in Paris.
      links: 
      - 'Collective website': http://rybn.org/thegreatoffshore/
  
   - 'Alisa Oleva':
      streamed: false
      type: 'walk'
      time: '17:00 - 18:00'
      location: 'Atoma'
      practical: |
         Starting point: Atoma

         [Free upon registration](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/399830470766)

      description: |
         **Listening to where you are not** 
         
         We will start together, joining a phone call. We will then split off and walk in any direction, listening to the description of what someone else is hearing. They will be somewhere in Ukraine. Somewhere where you are not. Somewhere where the soundscape has radically changed. Does that experience bring that place to us? Or does listening to someone’s descriptions of the sound of another place take us there?

         **See the 'Specials' section of the festival website for an interactive project by Alisa Oleva.**
      image: './images/AHbrVLYQ.jpeg'
      biography: |
         **Alisa Oleva** treats the city as her studio and urban life as material, considering issues of urban choreography and urban archeology, traces and surfaces, borders and inventories, intervals and silences, passages and cracks. Her projects have manifested as a series of interactive situations, performances, movements scores, personal and intimate encounters, parkour, walkshops, and audio walks.
      links:
      - 'Artist website': https://www.olevaalisa.com/
   - 'Collective Actions Group':
      streamed: true
      type: 'live'
      time: '19:00'
      location: 'Atoma'
      description: |
        This site-specific action for Oscillation Festival combines video and typescript documentation made in the Depot station on the Savelovskaya line in January 1990, originally made for an unrealised installation by Andrei Monastyrsky. In Oscillation, this video, as well as other typescript documentation from the series **Trips Out of Town**, serve as a setting for a reading choreography to be realised in collaboration with the event's organisers. **Performed by Sabine Hänsgen, Elena Biserna and Henry Andersen.**
      image: './images/f7V9iaVo.jpeg'
      biography: |
         A focal point in the development of Russian conceptual and performance art, **Collective Actions Group** came into being in 1976. The collective was founded by Andrei Monastyrski, Nikita Alexeev, Georgy Kizevalter, and Nikolai Panitkov. Later members include Elena Elagina, Igor Makarevich, Sergei Romashko, and Sabine Hänsgen. For several decades, the group organised Trips out of Town , in the course of which an empty field in the countryside around Moscow would often become the stage for minimal actions involving the participation of a close network of friends and collaborators. 
      links: 
      - 'Website collective': https://conceptualism.letov.ru/KD-ACTIONS.htm     
   - 'Bill Dietz ':
      streamed: true
      type: 'lecture performance'
      time: '19:30'
      location: 'Atoma'
      description: |
         **My Ears, The Police**


         If listening could play a role in rethinking the public sphere, its mediations, complicities, and normalizing mechanisms would first need to be better articulated. To that end, the legal, infrastructural, and conceptual limits of listening, both those local to Brussels and to the zombie remnants of 'modern listening' as such, are amplified into audibility.
      image: './images/0mNJj6Fk.jpeg'
      biography: |
         **Bill Dietz** is a composer, writer, and co-chair of Music/Sound in Bard College’s MFA program. His work is often presented in festivals, museums, apartment buildings, magazines, and on public streets. Recent publications include: Maryanne Amacher: Selected Writings and Interviews (co-edited with Amy Cimini, 2020), and Universal Receptivity (co-written with Kerstin Stakemeier, 2021).
      links: 
      - '#brusselssoundmap': https://www.youtube.com/watch?v=96KzBaBuu7A
      - 'Artist website': https://www.tutorialdiversions.org/
   - 'Ryoko Akama & Anne-F Jacques':
      streamed: true
      type: 'live'
      time: '20:00'
      location: 'Atoma'
      description: |
        Filaments warming up, the slow deformation of materials, thermostats as controllers. In this performance, Akama and Jacques explore heat as an active force at work around us. Through sound, light, and barely perceptible movement, temperature becomes a noticeable presence in the performance.
      image: './images/BgIEAeDk.jpeg'
      biography: |
         **Ryoko Akama** is a Japanese-Korean artist working with installation, performance and composition, presently residing in Huddersfield, UK. **Anne-F Jacques** is a sound artist based in Montreal, Canada, interested in amplification, erratic devices and trivial objects. Ryoko and Anne-F have been working together since 2016, in an ongoing collaboration that involves sharing ridiculous ideas, failed or dangerous experiments, and occasional performances and releases.
      links: 
      - 'Bandcamp': https://noticerecordings.bandcamp.com/album/evaporation
      - 'Vimeo': https://vimeo.com/247511818


   - 'Matana Roberts':
      streamed: true
      type: 'live'
      time: '21:15'
      location: 'Atoma'
      image: './images/cURayy2w.jpeg'
      biography: |
         **Matana Roberts** is an internationally renowned composer, band leader, saxophonist, sound experimentalist and mixed-media practitioner. Roberts works in many contexts and mediums, including improvisation, dance, poetry, and theater. She is perhaps best known for her acclaimed Coin Coin project, a multi-chapter work of 'panoramic sound quilting' that aims to expose the mystical roots and channel the intuitive spirit-raising traditions of American creative expression while maintaining a deep and substantive engagement with narrativity, history, community and political expression within improvisatory musical structures.
      links: 
      - 'Artist website': https://www.matanaroberts.com/
      - 'Bandcamp': https://matana-roberts.bandcamp.com/album/coin-coin-chapter-four-memphis
   - 'Marta de Pascalis':
      streamed: true
      type: 'live'
      time: '22:00'
      location: 'Atoma'
      description: ''
      image: './images/T4f2UB2A.jpeg'
      image_credits: '© Massimo Pegurri'
      biography: |
         **Marta De Pascalis** is an Italian musician and sound-designer based in Berlin. Her solo works employ analogue synthesis and a tape-loop system, which is used to create patterns of repetition that shape a sense of a dense, dynamic and cathartic distance. Her sound touches a wide range of electronic music genres, including ambient, Berlin school first excursions, psychedelic, and tape music.
      links: 
      - 'Artist website': http://www.martadepascalis.com/
      - 'Bandcamp': https://maesia.bandcamp.com/album/sonus-ruinae
   - 'Open Mic with Francesca Hawker':
      streamed: true
      type: 'live'
      time: '23:00'
      location: 'Atoma'
      description: |
         **The Open Mic Night** will presumably take the form of a collective stroll down Expectation Avenue, guided from one performance to the next by the assumptions of Francesca Hawker, who will hopefully have some prior knowledge about what will occur. Probably, the event will last for about an hour (excluding breaks) and 5 performers will have 5 minutes each to deliver a well-adjusted and surprising offering to the audience.

         **To contribute a performance to the event**, please send a 1 paragraph proposal, as well as technical requirements to **[margherita@q-o2.be](mailto:margherita@q-o2.be)** before April 22. Sets are limited to 5 minutes and should be acoustic, limited to a single microphone or able to be installed direct-to-mixer without sound check ('plug and play'). Performers will receive a ticket to the festival for Friday 29/4 and drink vouchers.

      image: './images/to0hn_oc.jpeg'
      image_credits: '© Nikolaj Jessen'
      biography: |
         **Francesca Hawker** is an artist from the UK who lives in Brussels. She predominantly performs, using her voice, recorded audio, and rudimentary props. She holds a Master’s degree from the Dutch Art Institute, an iterant roaming academy based in the Netherlands. Hawker is a current artist in residence at MORPHO in Antwerp.
      links: 
      - 'Artist website': https://francescahawker.net/
  
- '30.04':
   day_name: 'Saturday'
   events:
      
   - 'RYBN':
      streamed: false
      type: 'walk'
      time: '11:00 - 15:00'
      location: 'Location TBA'
      practical: |
         Capacity: 20

         [Registration required](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/736161239426)

         For a group price, please email [info@q-o2.be](mailto:info@q-o2.be)
      description: |
        The **Offshore Tour Operator** is a psycho-geographic GPS prototype that guides you through the 800,000 addresses of the ICIJ's Offshore Leaks database. The walks bring the participants to search for the physical traces of offshore banking within the architecture of various neighborhoods of the city of Brussels. Thus, the walks transform into a real hunt for shell companies, trust firms, domiciliation agencies and shadow finance offices and agents. At the end of each walk, a collective discussion offers a platform to participants to share their experiences and documents, in order to shape collectively an up to date image of finance that challenges the very notion of 'offshore'.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is an extradisciplinary artist collective, created in 1999 and based in Paris.
      links: 
      - 'Collective website': http://rybn.org/thegreatoffshore/

   - 'David Helbich':
      streamed: false
      type: 'self-performative installation'
      time: '14:00 - 16:00 (ongoing)'
      location: 'Abbaye de Forest'
      practical: |
         No registration required
      description: | 
         **Figures of Walking Together**
         
          The score of chalk on grass in up to three tracks for any number of people is a social choreography, with figures and patterns inspired by concepts of institutionally and intuitively organized walking in groups, such as in dance or military drill. In the end, it is the individual and collective behavior of the self-performing participants that sheds light on empowerment within such given structures. Chalk score & booklet, Brussels version 2022.

      image: './images/DavidHelbich.jpg'
      image_credits: '© David Helbich'
      biography: | 
         **David Helbich** is a sound-, installation- and performance artist, who creates a divers range of experimental and conceptual works for the stage, headphones, paper and online media, and in public space. His work moves between representative, interactive and conceptual approaches, often addressing concrete physical and social experiences. A recurrent interest is the interaction with a self-performing audience.
      links: 
      - 'Artist website': http://davidhelbich.blogspot.com/  
      
   - 'Alisa Oleva':
      streamed: false
      type: 'walk'
      time: '15:00 - 16:00'
      location: 'Zonneklopper'
      practical: |
         Starting point: Zonneklopper

         Capacity: 10

         [Free upon registration](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/971907879517)
      description: |
         **walk - stop - listen - walk again**


         A collective walk following a shared score. It is an invitation to tune in to each other, share and listen attentively. We will move as a group and whenever one of us hears something they want to stop and listen to, they do so, in an invitation for the entire group to stop and listen together. We stay there listening until someone else in the group decides to carry on walking. We repeat. We don’t know where we will finish, where the listening will take us.

         **See the 'Specials' section of the festival website for an interactive project by Alisa Oleva.**
      image: './images/mkSDHMHk.jpeg'
      biography: |
         **Alisa Oleva** treats the city as her studio and urban life as material, considering issues of urban choreography and urban archeology, traces and surfaces, borders and inventories, intervals and silences, passages and cracks. Her projects have manifested as a series of interactive situations, performances, movements scores, personal and intimate encounters, parkour, walkshops, and audio walks.
      links: 
      - 'Artist website': https://www.olevaalisa.com/

   - 'Pak Yan Lau & Amber Meulenijzer':
      streamed: false
      type: 'live'
      time: '16:00'
      location: 'Abbaye de Forest'
      description: |
        **SAAB SCULPTURES** is a series of works placing the Saab 900 car in different contexts—questioning, transforming, pimping public/private space. Travelling through different landscapes, new soundpieces are made-to-mesure for each performance. For this year's Oscillation Festival, Amber Meulenijzer and Pak Yan Lau will collaborate to create a composition and live performance tailored to this sound system.
      image: './images/HRrOLVm0.jpeg'
      biography: |
         **Pak Yan Lau**, born in Belgium, with roots from Hong Kong and now based in Brussels is a sound artist, improviser, musician and composer, who has developed over the years a rich, dense and captivating sound universe from prepared pianos, toy pianos, synths, electronics and various sound objects. Skilfully blending electro-acoustic approaches, she explores sound in a bewitching way, merging different sound sources with poetry, magic and finesse.

         **Amber Meulenijzer** has a background in sound and visual arts. She researches the tension between these fields, playing with presence/non presence, the role of silence and the spectator. Where do decor, installation and soundscape meet? What does the body need in order to listen? SAAB SCULPTURES is the first project translating this research into public space.

      links: 
      - 'Website Amber Meulenijzer': https://ambermeulenijzer.tumblr.com/
      - 'Website Pak Yan Lau': https://pakyanlau.com/              
   - 'De zwarte zusters':
      streamed: false
      type: 'live'
      time: '16:45'
      location: 'Abbaye de Forest'
      description: |
         An improvisation on sound, image and the narrative of De zwarte zusters as a community.
      image: './images/XOyZZSVE.jpeg'
      image_credits: '© Cato Van Rijckeghem'
      biography: |
         A group of young people have gathered around the name **De zwarte zusters**. During each performance, they explore in an improvisational manner the tension between the musical and the visual performative aspect, in which the search for collective values, friendship and trust are central. They like to be inspired by everyday objects, the environment and free play.
      links:
      - 'Artist website': http://zwartezusters.be/
         
   - 'Round Table':
      streamed: true
      type: 'talk'
      time: '18:00'
      location: 'Zonneklopper'
      description: |
         **Moving (in) Sound**

         The round table is an occasion to discuss together about formats and apparatuses that destabilize traditional relationships between artist and audience through movement, walks, itineraries, collective or private spatial explorations. How does this generate different ways of addressing the public? How does this redefine the way we inhabit the milieux we are part of? In which ways can this open up other spatial practices and representations? How does this interfere with the (physical, cultural, social, political) production of public space?

         With Alisa Oleva, Bill Dietz, RYBN and David Helbich.
         Moderated by Elena Biserna.
      image: './images/Chase28.jpg'
   - 'Céline Gillain':
      streamed: true
      type: 'talk'
      time: '20:00'
      location: 'Zonneklopper'
      description: |
         **How listening is conditioned by context**


         Céline will talk about how listening to music is conditioned by context, based on her own experience as a performer. Performing in a music festival, an art institution, a concert hall or a club are very different experiences, with different conditions, sets of rules and expectations. She's interested in questioning the cleavage this creates between audiences and how it is rooted in issues related to class, sex and race.
      image: './images/CVtiFED8.jpeg'
      image_credits: '© Claudia Höhne'
      biography: |
         **Céline Gillain** is an artist whose work explores the meeting points between different fields of art and music such as performance, storytelling, experimental and electronic music, deconstructing the codes and hierarchies that govern them. Exposing the ever growing precarity of the artist, her work questions the mechanisms by which ideas are transformed into commodities in mainstream culture.
      links: 
      - 'Artist website': https://linktr.ee/CelineGillain  
   - 'Peter Kutin with Stan Maris':
      streamed: true
      type: 'live'
      time: '20:30'
      location: 'Zonneklopper'
      description: |
         Peter Kutin will perform an improvised concert with the accordionist Stan Maris. The sound of the latter will be implemented and counterbalanced by Kutin’s Light-to-Noise Feedback Systems—sound and light will cast shadows.
      image: './images/Gh7mDYh0.jpeg'
      biography: |
         **Peter Kutin** works with sound across genres, medium and media. Driven by a bubbling curiosity underpinned by collaborative ethos, Kutin has written and developed music and sonic environments for film, theatre, performance, contemporary dance and radioplays, as well as directing several experimental short films himself.
         
         **Stan Maris** is a Belgian accordion player and composer, mainly active in the improvised music scene. He plays and composes for his own projects Kreis, Ocean Eddie, Erem and solo performances. As a sideman Maris is active in bands like Suura, Mathieu Robert Group and Giovanni Di Domenico's New Movement Ensemble.
      links: 
      - 'Website Peter Kutin': http://kutin.klingt.org/
      - 'Website Stan Maris': https://www.stanmaris.com/

   - 'Mariam Rezaei':
      streamed: true
      type: 'live'
      time: '21:30'
      location: 'Zonneklopper'
      description: |
         **BOWN** is experimental turntablism from composer and DJ Mariam Rezaei. Using two turntables, Mariam improvises with vocal and instrumental samples in an improvisation bridging her Iranian and English heritage. Timbres moves between reductionism, noise, free jazz and absurdism with hints of opera and hiphop.
      image: './images/ARTKJWWE.jpeg'
      biography: |
         **Mariam Rezaei** is an award winning composer, turntablist and performer. She leads experimental arts project TOPH in Gateshead, curating a regular series of concerts and festivals. Her newest releases include 'SKEEN' on Fractal Meat Cuts, 'The End of The World...Finally' with Sam Goff on Cacophonous Revival Recordings, 'il', a collaboration with Stephen Bishop on TUSK Editions and 'SISTER' with soprano Alya Al-Sultani, on Takuroku.
      links: 
      - 'Artist website': https://mariam-rezaei.com/
   - 'Thomas Ankersmit':
      streamed: true
      type: 'live'
      time: '22:15'
      location: 'Zonneklopper'
      description: |
        Thomas Ankersmit uses PA systems and his Serge Modular analog synthesizer to activate real, physical spaces and bodies with sound, or rather to suggest imaginary spaces, often in contrast to each other. Inspired by the work of e.g. Maryanne Amacher and Dick Raaijmakers, he explores the potential of resonant frequencies and oto-acoustic emissions to 'hack' simple stereo PA systems for a more physical, three-dimensional way of listening. Although he uses exclusively analogue-electronic sounds, the result nevertheless often has an organic, landscape-like nature.
      image: './images/P1VW05GQ.jpeg'
      image_credits: '© Mich Leemans'
      biography: |
         **Thomas Ankersmit** is a musician and sound artist based in Berlin and Amsterdam. He plays the Serge Modular synthesizer, both live and in the studio.His music is released on the Shelter Press, PAN, and Touch labels, and combines intricate sonic detail and raw electric power, with a very physical and spatial experience of sound.
      links: 
      - 'Artist website': https://thomasankersmit.net/
      - 'Soundcloud': https://soundcloud.com/weerzin/perceptual-geography-excerpt-live-at-ctm-festival-2019
   - 'DJ Marcelle':
      streamed: true
      type: 'live'
      time: '23:00'
      location: 'Zonneklopper'
      image: './images/rhkAjYyk.jpeg'
      biography: |
         'Although **DJ Marcelle** has been collecting music longer than most of us have been alive, she still feels like The Netherlands’ sweetheart. She’s an artist with a mischievous, rulebending and almost ironic approach to her DJing, producing and radio hosting–cut with her trademark wit. Well known for her three-turntable setup, DJ Marcelle makes compositions out of songs and symphonies out of mixes — colliding disparate genres, appropriated vocal snippets and warped soundscapes into a giant Frankenstein-like melting pot. Her performances are inventive, euphoric and above all powerful.' — Resident Advisor
      links:
      - 'Artist website': https://filhounico.com/booking/84

- '01.05':
   day_name: 'Sunday'
   events:
      
   - 'RYBN':
      streamed: false
      type: 'walk'
      time: '11:00 - 15:00'
      location: 'Location TBA'
      practical: |
         Capacity: 20

         [Registration required](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/644099568474)

         For a group price, please email [info@q-o2.be](mailto:info@q-o2.be)
      description: |
        The **Offshore Tour Operator** is a psycho-geographic GPS prototype that guides you through the 800,000 addresses of the ICIJ's Offshore Leaks database. The walks bring the participants to search for the physical traces of offshore banking within the architecture of various neighborhoods of the city of Brussels. Thus, the walks transform into a real hunt for shell companies, trust firms, domiciliation agencies and shadow finance offices and agents. At the end of each walk, a collective discussion offers a platform to participants to share their experiences and documents, in order to shape collectively an up to date image of finance that challenges the very notion of 'offshore'.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is an extradisciplinary artist collective, created in 1999 and based in Paris.
      links: 
      - 'Collective website': http://rybn.org/thegreatoffshore/
   - 'Jérôme Giller':
      streamed: false
      type: 'walk'
      time: '13:30 - 15:30'
      location: 'Union tram stop'
      practical: |
         Capacity: 20

         [Registration required](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/352094840913)

         For a group price, please email [info@q-o2.be](mailto:info@q-o2.be)
      description: |
        **Forest Village: Archipelago of Habitats**

        In this walk, Jérôme Giller proposes to survey the bottom of Forest by connecting blocks of habitation that allow us to understand the historical, sociological and economic evolution of of the industrial area, distributed around the railway line 124 which connects Brussels to Charleroi. 

        Starting point of the walk:
        Union tram stop (line 82 and 97): Avenue Van Volxem 208 / 1190 Forest
      image: './images/CC7gWQg0.jpeg'
      image_credits: '© Archives Jérôme Giller'
      biography: |
         **Jérôme Giller** lives and works in Brussels where he leads a reflection on urban and peri-urban territorialities using walking as a method and tool of artistic creation. He surveys the territories following lines of geographical, urban, historical, and poetic wanderings. Giller's interventions are furtive and immaterial. These are moments to live and experience; laboratories that infiltrate reality.
      links: 
      - 'Artist website': http://www.jeromegiller.net/
   - 'David Helbich':
      streamed: false
      type: 'self-performative installation'
      time: '14:00 - 16:00'
      location: 'Abbaye de Forest'
      description: | 
         
         **Figures of Walking Together** - No registration required
         
          The score of chalk on grass in up to three tracks for any number of people is a social choreography, with figures and patterns inspired by concepts of institutionally and intuitively organized walking in groups, such as in dance or military drill. In the end, it is the individual and collective behavior of the self-performing participants that sheds light on empowerment within such given structures. Chalk score & booklet, Brussels version 2022.

      image: './images/DavidHelbich.jpg'
      image_credits: '© David Helbich'
      biography: | 
         **David Helbich** is a sound-, installation- and performance artist, who creates a divers range of experimental and conceptual works for the stage, headphones, paper and online media, and in public space. His work moves between representative, interactive and conceptual approaches, often addressing concrete physical and social experiences. A recurrent interest is the interaction with a self-performing audience.
      links: 
      - 'Artist website': http://davidhelbich.blogspot.com/  
      
   - 'Lia Mazzari':
      streamed: false
      type: 'live'
      time: '16:00'
      location: 'Abbaye de Forest'
      description: |
         **Whipping Music**
         
         An unconventional device for sound production, turned-into instrument. A visceral phenomenon, cathartic and often overly fetishized extension of the body and listening machine, the whip crack produces a sonic boom. Lia Mazzari will host a workshop prior to the festival, teaching participants to swing and crack whips as an investigatory sonic mapping device to activate the architectures and sites around us. For this performance, the participants will perform their sonic choregraphy of whip cracks.
      image: './images/UGj7yikg.jpeg'
      biography: |
         **Lia Mazzari** engages new audiences through encounters with art in non-conventional spaces via performance, installation and intervention. She creates recorded/live events that discuss ways in which sound can be used as a multidimensional force of resilience and acoustic commoning. This relationship towards a sonic activism often engages environmental recording, instrumentation, voice and transmission technologies.
      links: 
      - 'Artist website': https://liamazzari.com/
   - 'BMB con.':
      streamed: false
      type: 'live'
      time: '16:30'
      location: 'Abbaye de Forest'
      description: |
        BMB con. is developing a new site-specific performance for the Oscillation festival.
      image: './images/zB0NNcvc.jpeg'
      biography: |
         For the last 30-odd years, **BMB con.** have been performing a balancing act between music and noise, sound and vision, indoors and out, digital and analogue, performer and audience. BMB con. eas founded by Roelf Toxopeus, Justin Bennett and Wikke ‘t Hooft in 1989. Since 2006 it consists of a core duo working together with a changing group of invited artists/performers. They incorporate electronic and acoustic music, film, video and physical theatre in their performances and installations.
      links: 
      - 'Artist website': http://www.bmbcon.org/
   - 'Label Market & Bookshop':
      streamed: false
      type: 'live'
      time: '17:00 - 21:00'
      location: 'Zonneklopper'
      description: |
         A record fair and bookshop by our favourite local independent labels and the nomadic bookshop **Underbelly**.
      biography: |
   - 'RYBN':
      streamed: true
      type: 'listening session'
      time: '17:30'
      location: 'Zonneklopper'
      description: |

         RYBN will be summarising their experiences of the past week, realising the Offshore Tour Operator in Brussels.

         The **Offshore Tour Operator** is a psycho-geographic GPS prototype that guides you through the 800,000 addresses of the ICIJ's Offshore Leaks database. The walks bring the participants to search for the physical traces of offshore banking within the architecture of various neighborhoods of the city of Brussels. Thus, the walks transform into a real hunt for shell companies, trust firms, domiciliation agencies and shadow finance offices and agents. At the end of each walk, a collective discussion offers a platform to participants to share their experiences and documents, in order to shape collectively an up to date image of finance that challenges the very notion of 'offshore'.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** is an extradisciplinary artist collective, created in 1999 and based in Paris.
      links: 
      - 'Collective website': http://rybn.org/thegreatoffshore/
   - 'Leandro Pisano':
      streamed: true
      type: 'talk'
      time: '18:00'
      location: 'Zonneklopper'
      description: |
       **The Manifesto of Rural Futurism** is an invitation to experience rural locations as spaces in which to question our approach to history and landscape. In this text, written in 2019 by Leandro Pisano and Beatrice Ferrara, listening practices are deployed as ways to critically traverse the 'border territories' of rural space, challenging persisting notions about 'inescapable marginality', 'residuality' and 'peripherality'.
      image: './images/oh-uMGU0.jpeg'
      biography: |
         **Leandro Pisano** is a curator, writer and independent researcher who is interested in intersections between art, sound and technoculture. The specific area of his research deals with political ecology of rural and marginal territories. He is founder of Interferenze/Liminaria festival and has curated sonic arts exhibitions in Italy, Chile and Australia.
      links: 
      - 'Youtube': https://www.youtube.com/watch?v=7WqDmUp9SHU&feature=youtu.be&ab_channel=Liminaria
      - 'Artist website': https://www.leandropisano.it/en/
   - 'Fausto Cáceres (Shirley & Spinoza)':
      streamed: true
      type: 'live'
      time: '18:30'
      location: 'Zonneklopper'
      description: |
         **Street Cries & the Wandering Song** is a lyrical collage of the texture and life that traversed the little cobblestone intersection beneath my studio window in Dali, China. The melodic cries of peddlers, recyclers and spontaneous scenes were captured between 2006 and 2020, primarily with a stereo microphone suspended above the street or with binaural mics in other locations. This is a remote performance live from New Zealand.
      image: './images/ROGC_sandangel.jpg'
      biography: |
         **Fausto Cáceres** is an American sound creator/collector who recently relocated to New Zealand after living 15 years in the peripheries of mainland China where he extensively documented traditional music of minority cultures as well as the ever transforming soundscape of the People's Republic of China. Cáceres is also the 'Remote Operator' of the long-running Shirley & Spinoza Radio.
      links: 
      - 'Artist website': https://beacons.ai/shirleyandspinoza      
   - 'Jasmine Guffond':
      streamed: true
      type: 'live'
      time: '19:00'
      location: 'Zonneklopper'
      description: |
         **Listening to Listening to Listening** is an installative performance whereby Jasmine Guffond performs cut ups, random loopings and pitch manipulations of field recordings made by Margherita Brillada in and around the Zonneklopper building. Transmitted via transducers attached to specific points within Zonneklopper’s Salle Mouvement, sound’s potential as a vibrational force is activated to resonate the materiality of the room itself. Sound at once singular and universal affects individual bodies and matter uniquely while enveloping all at once through different intensities. What could it mean to listen collectively yet non-equivalently as the audience is invited to listen not only to the building but to Jasmine listening to Margherita. Could listening to listening be a technique for the appreciation of difference?
      image: './images/jasmine-guffond.jpg'
      biography: |
         **Jasmine Guffond** is an artist and composer working at the interface of social, political and technical infrastructures. Focused on electronic composition across music and art contexts her practice spans live performance, recording, installation and custom made browser add-on. Through the sonification of data she addresses the potential of sound to engage with contemporary political questions and engages listening as a situated knowledge practice.
      links: 
      - 'Artist website': http://jasmineguffond.com/
         
   - 'Aymeric de Tapol':
      streamed: true
      type: 'live'
      time: '20:00'
      location: 'Zonneklopper'
      description: |
         Aymeric de Tapol writes (read aloud in your head): 'This music is based on the listening of a cluster composed by written sequences for analogue synthesizer. It is the first time that the music seems to me to be the phenomenon itself, meaning that it is more at the centre of its history and is always questionable. In short, it is the observation of a repetition of moving sounds: the polyrhythm.'
      image: './images/BpzCNIdA.jpeg'
      biography: |
         **Aymeric de Tapol** makes field recordings and music that gets labelled as 'experimental'. He collaborates with other artists working in cinema, documentary, independent radio and performance. De Tapol is a member of the duo 'Cancellled' with Yann Leguay as well as the collective 'p-node'. His music has been released by independent labels such as Tanuki, Angström, Vlek, Lexi disque, Tanzprocesz and Knotwilg records.
      links: 
      - 'Bandcamp': https://aymeridetapol.bandcamp.com/track/concert-biennale-du-mans-2022
---
