---
title: About
default_state: 'off'
column: 3
order: 4
overflow: scroll
---

**The fourth edition of the Oscillation festival** is dedicated to the question what it means to address a public, and how, by changing the site of a performance, the relationship between audience and performer shifts. Inspired by the transitory nature of public space, many artists are drawn to formats which resist the demands of the traditional performer/audience dynamic. Experimental formats such as soundwalks, itinerant performances and virtual events, allow work to move into public spaces where it interacts with a changing and unstable environment. A public is not necessarily a crowd, and experiments with small scale and outside formats are proving resilient, and raising meaningful questions about notions such as public and private, environment and nature, aspects of introspection and connection.

**Public Address System** (PA), is an umbrella term for a set of technologies that allow a person to address multiple people simultaneously, typically via a system of microphones and speakers, such as in a venue, train station, or supermarket. In each case, the system’s design reflects and defines the mode of address. In the examples above, a single speaker addresses many listeners who cannot speak back, defining a power relationship. As systems of address are shifting online, the model of a single speaker with multiple receivers has increasingly fractured, producing new and often contradictory modes of publicness. By taking the term public address as a banner for this year’s Oscillation festival, we want to ask: what other systems of address are possible, and what kinds of public might they speak to? By experimenting with the system of address itself can we find other ways of being public?
 

**The festival** will explore a number of alternative formats for sound in public space, interpolated by concerts which try to unpick the question of public address from within a more traditional dynamic. These experiments are grounded in the research of historian Elena Biserna, with whom we are developing a publication due in Autumn 2022. The publication will serve as an epilogue to the festival’s four days of walks, performances, talks and workshops.
